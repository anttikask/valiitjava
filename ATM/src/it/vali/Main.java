package it.vali;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

public class Main {

    static String pin;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int balance = loadBalance();
        pin = loadPin();

        savePin(pin);

        saveBalance(balance);

        if (!validatePin()){
            System.out.println("Kaart konfiskeeritud");
            return;
        }


        if (validatePin()) {
            System.out.println("Vali toiming:\n" +
                    " a) Sularaha sissemakse\n" +
                    " b) Sularaha väljamakse\n" +
                    " c) Kontojäägi vaatamine\n" +
                    " d) PIN koodi muutmine\n" +
                    " e) Konto väljavõte");
            String answer = scanner.nextLine();
            switch (answer) {
                case "a":
                    System.out.println("Kirjuta summa, mida soovid automaati sisestada");
                    int amount = Integer.parseInt(scanner.nextLine());
                    saveBalance(balance + amount);
                    accountBalance(amount);
                    System.out.println("Summa kontole kontole lisatud!");
                    break;
                case "b":
                    System.out.println("Vali summa:\n" +
                            " a) 5€\n" +
                            " b) 10€\n" +
                            " c) 20€\n" +
                            " d) Muu summa\n");
                    String enteredAmount = scanner.nextLine();
                    switch (enteredAmount) {
                        case "a":
                            System.out.println("Võta automaadist raha");
                            amount = 5;
                            saveBalance(balance - amount);
                            accountBalance(-(amount));
                            break;
                        case "b":
                            System.out.println("Võta automaadist raha");
                            amount = 10;
                            if (balance < amount){
                                System.out.println("Pole piisavalt vahendeid");
                            }
                            else{
                                saveBalance(balance - amount);
                                accountBalance(-(amount));
                            }
                            break;
                        case "c":
                            System.out.println("Võta automaadist raha");
                            amount = 20;
                            saveBalance(balance - amount);
                            accountBalance(-(amount));
                            break;
                        case "d":
                            System.out.println("Sisesta summa");
                            amount = Integer.parseInt(scanner.nextLine());
                            saveBalance(balance - amount);
                            accountBalance(-(amount));
                            System.out.println("Võta automaadist raha");
                            break;
                    }
                case "c":
                    System.out.printf("Teie kontojääk on hetkel: %d€", loadBalance());
                    break;
                case "d":
                    for (int i = 0; i < 3; i++) {
                        System.out.println("Sisestage kehtiv PIN kood");
                        String enteredPin = scanner.nextLine();
                        if(enteredPin.equals(pin)){
                            System.out.println("Sisestage uus PIN kood");
                            String newPin = scanner.nextLine();
                            savePin(newPin);
                            System.out.println("Uus PIN salvestatud");
                            break;
                        }
                        else{
                            System.out.println("Kood on vale, Teie sessioon on lõppenud");
                        }
                    }
                case "e":
                    printTransactions();
                    break;
                default:
                    break;
                    }
        }
        else{
            System.out.println("Kood on vale, Teie sessioon on lõppenud!");
        }
    }

    static String currentDateTimeToString() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

        Calendar calendar = Calendar.getInstance();

        Date date = new Date();
        return dateFormat.format(date);
    }

    static void savePin(String pin) {
        // Toimub pin kirjutamine pin.txt faili
        try {
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\GitHub\\valiitjava\\ATM\\pin.txt");
            fileWriter.write(pin + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("PIN koodi salvestamine ebaõnnestus");
        }
    }

    static void saveBalance(int balance) {
        // Toimub balance kirjutamine balance.txt faili
        try {
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\GitHub\\valiitjava\\ATM\\balance.txt");
            fileWriter.write(balance + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontojäägiga on jama");
        }
    }

    static int loadBalance() {
        // Toimub balance välja lugemine balance.txt failist
        int balance = 0;
        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\GitHub\\valiitjava\\ATM\\balance.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            balance = Integer.parseInt(bufferedReader.readLine());
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi laadimine ebaõnnestus");
        }
        return balance;
    }

    static String loadPin() {
        // Toimub pin välja lugemine pin.txt failist
        String pin = null;
        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\GitHub\\valiitjava\\ATM\\pin.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            pin = bufferedReader.readLine();
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pin;
    }

    static boolean  validatePin(){
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < 3; i++) {
            System.out.println("Palun sisesta PIN kood");

            String enteredPin = scanner.nextLine();

            if (enteredPin.equals(pin)){
                System.out.println("Tore! Õige PIN kood");
                return true;
            }
        }
        return false;
    }

    static void accountBalance(int amount) {
        // Toimub konto väljavõtte kirjutamine Kontovv.txt faili
        try {
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\GitHub\\valiitjava\\ATM\\Kontovv.txt", true);
            fileWriter.append(amount  + " " + currentDateTimeToString() + System.lineSeparator());;
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void printTransactions(){
        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\GitHub\\valiitjava\\ATM\\Kontovv.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String tehing = bufferedReader.readLine();
            int lineNumber = 0;
            while (tehing != null) {
                System.out.println(tehing);
                tehing = bufferedReader.readLine();
            }
            bufferedReader.close();
            fileReader.close();
        } catch(IOException e){
            System.out.println("Konto väljavõtte laadimine ebaõnnestus");
        }
    }

}

