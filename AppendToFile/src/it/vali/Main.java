package it.vali;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            FileWriter fileWriter = new FileWriter("output.txt", true);
            fileWriter.append("Tere\r\n");

            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 1. kodune ül - koosta täisarvude massiiv 10-st arvust ning seejärel kirjuta faili kõik suuremad arvud, kui 2.

        // 2. kodune ül - küsi kasutajalt kaks arvu. Küsi seni kuni mõlemad on korrektsed (on numbriks teisendatavad) ning nende summa ei ole paaris arv.
        // 3. kodune ül - küsi kasutajalt mitu arvu ta tahab sisestada (nt. 7), seejärel küsi ühe kaupa kasutajalt need arvud ning kirjuta nende arvude summa faili
        // nii et see lisatakse alati juurde (appendiga).
    }
}
