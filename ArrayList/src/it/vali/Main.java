package it.vali;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        // Koostame ArrayListi suvaliste väärtustega
        // Tavalisse ArrayListi võime lisada ükskõik mis tüüpi elemente, aga elemente küsides sealt ArrayListis,
        // pean teadma, mis tüüpi element kus täpselt asub ning pean selleks tüübiks küsimisel ka cast'ima.
        List list = new ArrayList();

        list.add("tere");
        list.add(23);
        list.add(false);

        // Koostame suvalised parameetrid
        double money = 24.55;
        Random random = new Random();

        // Lisame need parameetrid listi
        list.add(money);
        list.add(random);

        // seame muutuja a väärtuseks listi väärtuse kohal (index) 1
        int a = (int) list.get(1);

        // seame stringi muutuja word väärtuseks listi väärtuse kohal 0
        String word = (String) list.get(0);

        // Listi väärtusel kohal 1 peame selle igal juhul int'iks castima enne, kui seda 3-le liita tahame
        int sum = 3 + (int) list.get(1);

        // Saame küsida mingil kohal oleva indexi väärtuse tüüpi
        if(Integer.class.isInstance(a)){
            System.out.println("a on int");
        }
        if(Integer.class.isInstance(list.get(1))){
            System.out.println("listis indeksiga 1 on int");
        }
        if(String.class.isInstance(list.get(0))){
            System.out.println("listis indeksiga 0 on string");
        }

        // Add meetodi võimalus lisada indexile kohal 2 uus element "head aega"
        list.add(2, "head aega");

        for (int i = 0; i < list.size() ; i++) {
            System.out.println(list.get(i));
        }

        // Saame kõik elemendid korraga ühest listis teise lisada addAll meetodiga

        List otherList = new ArrayList();
        otherList.add(1);
        otherList.add("maja");
        otherList.add(true);

        list.addAll(otherList);

        System.out.println();

        for (int i = 0; i < list.size() ; i++) {
            System.out.println(list.get(i));
        }

        // contains meetod kontrollib, kas mingi element listis sisaldub

        if (list.contains(money)){
            System.out.println("money asub listis");
        }

        // indexOf meetodiga saab kontrollida mingi elemendi asukohta
        System.out.printf("23 asub listis indeksiga %d%n", list.indexOf(23));

        // isEmpty kontrollib, kas list on tühi või mitte - kui on tühi, siis tagastab true ja vastupidi, siis false
        list.isEmpty();

        // remove abil saame eemaldada mingi elemendi listist või eemaldada mingi indeksi
        list.remove(money);
        list.remove(2);

        // set meetod asendab konkreetse elemendi konkreetsesse indeksisse ehk kirjutab üle (add meetod paneb lõppu või vahele)
        list.set(2, "tere2");

    }
}
