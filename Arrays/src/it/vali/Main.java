package it.vali;

public class Main {

    public static void main(String[] args) {
        // Järjend, massiiv, nimekiri - ühes muutujas saab hoida sama tüüpi elemente mitu tükki

        //Luuakse täisarvude massiiv, millesse mahub 5 elementi. Loomise hetkel määratud elementide arvu hiljem muuta ei saa
        int[] numbers = new int[5];

        //Massiivi indeksid algavad nullist mitte ühest.
        //Viimane indeks on alati ühe võrra väiksem, kui massiivi pikkus.
        //Määrame indeksitele väärtused;
        numbers[0] = 2;
        numbers[1] = 7;
        numbers[2] = -2;
        numbers[3] = 11;
        numbers[4] = 1;

        System.out.println(numbers[0]);
        System.out.println(numbers[1]);
        System.out.println(numbers[2]);
        System.out.println(numbers[3]);
        System.out.println(numbers[4]);

        System.out.println(); //vaherida

        //For tsükliga massiivi elementide välja printimine.
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        //For tsükliga massiivi elementide välja printimine.
        for (int i = 4; i >= 0; i--) {
            System.out.println(numbers[i]);
        }

        System.out.println();

        //Prindi numbrid, mis on suuremad, kui 2:
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > 2) {
                System.out.println(numbers[i]);
            }
        }

        System.out.println();

        //Prindi kõik paarisarvud:
        for (int i = 0; i < 5; i++) {
            if (numbers[i] % 2 == 0) {
                System.out.println(numbers[i]);
            }
        }

        System.out.println();

        //Prindi tagant poolt 2 esimest paaritut arvu:
        int counter = 0;
        for (int i = 4; i >= 0; i--) {
            if (!(numbers[i] % 2 == 0)) {
                System.out.println(numbers[i]);
                counter++;
                    if(counter == 2){
                        break;
                    }
            }
        }

        System.out.println();

        //Loo teine massiiv 3-le numbrile ja pane sinna esimesest massiivist kolm esimest numbrit ja prindi teise massiivi elemendid ekraanile:

        int[]numbersTwo = new int[3];

        for (int i = 0; i < numbersTwo.length ; i++) {
            numbersTwo[i] = numbers[i];
            System.out.println(numbersTwo[i]);
        }

        System.out.println();

        //Loo kolmas massiiv 3-le numbrile ja pane sinna esimesest massiivist 3 numbrit tagant poolt alates (1, 11, -2)

        int[]numbersThree = new int[3];

        for (int i = 0; i < numbersTwo.length ; i++) {
            numbersTwo[i] = numbers[numbersTwo.length - i - 1];
            System.out.println(numbersTwo[i]);
        }

        System.out.println();
        //Loome kolmanda massiivi ning kasutame indeksi jälgimiseks uut abimuutujat j.

        int[] thirdNumbers = new int[3];

        for (int i = 0, j = numbers.length-1; i < thirdNumbers.length ; i++, j--) {
            thirdNumbers[i] = numbers[j];
        }
        for (int i = 0; i < thirdNumbers.length ; i++) {
            System.out.println(thirdNumbers[i]);
        }
    }
}
