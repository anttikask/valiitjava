// Defineeri klass Country, millel oleksid meetodid getPopulation, setPopulation, geName, setName ja list riigis
// enim kõneldavate keeltega. Override'i selle klassi toString() meetod nii, et selle meetodi väljakutsumine tagastaks
// teksti, mis sisaldaks endas kõiki parameetreid väljaprindituna.

// Tekita antud klassist üks objekt ühe vabalt valitud riigi andmetega ja prindi selle riigi info välja .toString()
// meetodi abil

package it.vali;

import java.util.List;

public class Country {

    private String population;
    String name;
    private List<String> languages;

    public List<String> getLanguages() {

        return languages;
    }

    public void setLanguages(List<String> languages) {

        this.languages = languages;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return String.join(", ", name, population, String.join(", ",languages));
    }




}
