package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // 1. Deklareeri muutuja, mis hoiaks endas PI arvulist väärtust vähemalt 11 kohta peale koma. Korruta selle muutuja
        // väärtus kahega ja prindi välja standardväljundisse.

        float a = 2 * ((float) Math.PI);
        System.out.printf("Muutuja väärtus on %.11f", a);

        System.out.println();

        // 2. ülesande kontroll
        int b = 2;
        int c = 3;
        boolean areEqual = equalNumbers(b, c);

        System.out.printf("Kas numbrid %d ja %d on võrdsed? %s\n", b, c, areEqual);

        // 3. ülesanne

        String[] words = new String[] {"tere", "kuidas", "sul", "läheb", "täna"};

        System.out.println();

        // 4. ülesanne

        System.out.println(yearTest(0));
        System.out.println(yearTest(1));
        System.out.println(yearTest(128));
        System.out.println(yearTest(598));
        System.out.println(yearTest(1624));
        System.out.println(yearTest(1827));
        System.out.println(yearTest(1996));
        System.out.println(yearTest(2017));

        System.out.println();

        // 5. ülesanne

        List<String> languages = new ArrayList<String>();
        languages.add("Eesti");
        languages.add("Vene");
        languages.add("Ukraina");
        languages.add("Läti");


        Country Eesti = new Country();

        Eesti.setName("Eesti");
        Eesti.setPopulation("1500000");
        Eesti.setLanguages(languages);

        System.out.println(Eesti.toString());

    }

    // 2. Kirjuta meetod, mis tagastab boolean-tüüpi väärtuse ja mille sisendparameetriteks on kaks täisarvulist muutujat.
    // Meetod tagastab tõeväärtuse vastavalt sellele, kas kaks sisendparameetrit on omavahel võrdsed või mitte. Meetodi nime
    // võid ise välja mõelda

    static boolean equalNumbers (int a, int b) {
        if (a == b){
            return true;
        }
        return false;
    }

    // 3. Kirjuta meetod, mille sisendparameetriks on Stringide massiiv ja mis tagastab täisarvude massiivi. Tagastatava
    // massiivi iga element sisaldab endas vastava sisendparameetrina vastu võetud massiivi elemendi (stringi) pikkust.
    // Meetodi nimi endal välja mõelda.

    static int[] arrayTest(String[] words){
        int [] numbers = new int[words.length];

        for (int i = 0; i < words.length; i++) {
            numbers[i] = words[i].length();
        }
        return numbers;
    }

    // 4. Kirjuta meetod, mis võtab sisendparameetriks aastaarvu täisarvulisel kujul (int) vahemikus 1 - 2018 ja tagastab
    // täisarvu (byte) vahemikus 1 - 21 vastavalt sellele, mitmendasse sajandisse antud aasta kuulub. Meetodi nime või ise välja mõelda
    // Kui funktsioonile antakse ette parameeter, mis on kas suurem, kui 2018 või väiksem, kui 1, tuleb tagastada väärtus -1.

    // Kutsu see meetod välja main()-meetodist järgmiste erinevate sisendväärtustega: 0, 1, 128, 598, 1624, 1827, 1996, 2017.
    // Prindi need väärtused standardväljundisse.

    static byte yearTest(int year){

        if (year > 1 || year < 2018){

            return (byte) (year/100 + 1);
        }
        else {
            return -1;
        }
    }

    // Defineeri klass Country, millel oleksid meetodid getPopulation, setPopulation, geName, setName ja list riigis
    // enim kõneldavate keeltega. Override'i selle klassi toString() meetod nii, et selle meetodi väljakutsumine tagastaks
    // teksti, mis sisaldaks endas kõiki parameetreid väljaprindituna.

    // Tekita antud klassist üks objekt ühe vabalt valitud riigi andmetega ja prindi selle riigi info välja .toString()
    // meetodi abil



}
