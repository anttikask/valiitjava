package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
//        //Küsime kasutajalt PIN-koodi, kui see on õige, ütleme "tore" muul juhul küsime uuesti
//        //aga kokku küsime ainult 3 korda.
//
        String realPin = "1234";

        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < 3; i++) {
            System.out.println("Palun sisestage Pin-kood");

            String enteredPin = scanner.nextLine();

            if(enteredPin.equals(realPin)){
                System.out.println("Tore, õige Pin kood");
                break;
            }
        }
        //Sama ülesanne do-while'ga
        int numberOfGuesses = 3;

        do {
            System.out.println("Palun sisestage Pin-kood");
            numberOfGuesses--;
        } while(!(scanner.nextLine().equalsIgnoreCase(realPin)) && numberOfGuesses > 0);

        if(numberOfGuesses == 0) {
            System.out.println("Panid koodi 3 korda valesti");
        }
        else {
            System.out.println("Tore! Õige pin kood");
        }

        //Prindi välja 10 kuni 20 ja 40 kuni 60 kasutades continue keyword'i
        //Continue jätab selle tsükli korduse katki ja läheb järgmise korduse juurde

        for (int i = 10; i <= 60 ; i++) {
            if(i > 20 && i < 40) {
                continue;
            }
            System.out.println(i);
        }
    }
}

