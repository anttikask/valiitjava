package it.vali;

import java.lang.reflect.Array;

public class Main {

    public static void main(String[] args) {
        int sum = sum(3, 4);
        System.out.printf("Arvude 3 ja 4 summa on %d\n", sum);

        int sub = sub(3, 4);
        System.out.printf("Arvude 3 ja 4 lahutis on %d\n", sub);

        double div = div(3.0, 4.0);
        System.out.printf("Arvude 3 ja 4 jagatis on %f\n", div);

        double mult = mult(8.0, 10.0);
        System.out.printf("Arvude 8 ja 10 korrutis on %f\n", mult);

        int[] numbers = new int[]{1, 2, -2};
        sum = sum(numbers);
        System.out.printf("Massiivi arvude summa on %d%n", sum);

        numbers = new int[]{1, 2, 3, 4, 5};
        int[] reversed = reverseNumbers(numbers);

        printNumbers(convertToIntArray(new String[] {"2", "-12", "1", "0", "17"}));   // Loome jooksu pealt täisarvu massiivi, mis läheb meetodi parameetriks

        // 1. kodune ülesanne
        String[] words = new String[]{"tere", "kuidas", "sul", "läheb", "täna"};

        // 2. kodune ülesanne
        int[] numbers2 = new int[] {1, 5, 6, 12, 54};
        System.out.printf("Arvude keskmine on %.2f\n", average(numbers2));

        // 4. kodune ülesanne
        double number1 = 3;
        double number2 = 4;
        double percentage = percentage(number1, number2);
        System.out.printf("%.2f moodustab %.2f-st %.2f%%\n", number1, number2, percentage(number1,number2));

        // 5. kodune ülesanne
        double radius = 5;
        System.out.printf("Ringi ümbermõõt on %.2f", circumferenceOfCircle(radius));
    }

    // Meetod, mis liidab 2 täisarvu kokku ja tagastab nende summa
    static int sub(int a, int b) {
        int sub = a - b;
        return sub;
    }

    // Meetod, mis lahutab 2 täisarvu ja tagastab nende summa
    static int sum(int a, int b) {
        int sum = a + b;
        return sum;
    }

    // Meetod, mis jagab 2 arvu ja tagastab vastuse.
    static double div(double a, double b) {
        double div = a / b;
        return div;
    }

    // Meetod, mis korrutab 2 arvu ja tagastab vastuse.
    static double mult(double a, double b) {
        double mult = a * b;
        return mult;
    }

    // Meetod, mis võtab parameetriteks täisarvude massiivi ja liidab elemendid kokku ning tagastab summa:
    static int sum(int[] numbers) {
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }

    // Prindi välja täisarvu massiivi elemendid:
    static void printNumbers(int[] numbers){
        for (int i = 0; i < numbers.length ; i++) {
            System.out.println(numbers[i]);
        }
    }


    // Meetod, mis võtab parameetriks täisarvude massiivi, pöörab tagurpidi ja tagastab selle
    static int[] reverseNumbers(int[] numbers) {
        int[] reversedNumbers = new int[numbers.length];

        for (int i = 0; i < numbers.length; i++) {
            reversedNumbers[i] = numbers[numbers.length - i - 1];
        }
        return reversedNumbers;
    }


    // Meetod, mis võtab parameetriks stringi massiivi (eeldusel, et tegelikult on seal massiivis on numbrid stringidena)
    // ja teisendab numbrite massiviks ning tagastab selle.
    static int[] convertToIntArray(String[] numbersAsText){
        int[] numbers = new int[numbersAsText.length];

        for (int i = 0; i <numbers.length; i++) {
            numbers[i] = Integer.parseInt(numbersAsText[i]); // teisendame Stringina sisestatud massiivi täisarvudeks.
        }
        return numbers;
    }


    // 1. Meetod, mis võtab parameetriks stringi massiivi ning tagastab lause, kus iga stringi vahel on tühik.

    static String arrayToString(String []sentence) {
        return String.join("", sentence);
    }
    // 2. Meetod, mis võtab parameetriks stringi massiivi ning teine parameeter on sõnade eraldaja. Tagastada lause.
    static String arrayToString(String delimiter, String[] sentence){
        return String.join(delimiter, sentence);
    }

    // 3. Meetod, mis leiab numbrite massivist keskmise ning tagastab selle.
    static double average(int[] numbers2){
        double sum = 0;
        for (int i = 0; i < numbers2.length; i++) {
                sum += numbers2[i];
        }
        double average = sum / numbers2.length;
        return average;
    }

    // 4. Meetod, mis arvutab mitu protsenti moodustab esimene arv teisest.
    static double percentage(double number1, double number2) {
        return number1 / number2 * 100;
            }

    // 5. Meetod, mis leiab ringi ümbermõõdu raadiuse järgi.
    static double circumferenceOfCircle(double radius){
        return 2 * radius * Math.PI;
            }
}
