package it.vali;

import java.util.Scanner;

public class Main {
    // Küsi kasutajalt 2 arvu ja seejärel küsi kasutajalt, mis tehet soovib teha (a)liitmine, (b)lahutamine, (c)korrutamine, (d)jagamine)
    // Prindi kasutajale tehte vastus
    public static void main(String[] args) {
        String answer = "";
        boolean wrongAnswer;
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("Sisesta esimene täisarv");
            int a = Integer.parseInt(scanner.nextLine());
            System.out.println("Sisesta teine täisarv");
            int b = Integer.parseInt(scanner.nextLine());
            System.out.println("Millist tehet soovid teha nendega arvudega? Vajuta sobivat tähte\n" +
                    "a. Liitmine\n" +
                    "b. Lahutamine\n" +
                    "c. Korrutamine\n" +
                    "d. Jagamine");

            answer = scanner.nextLine();
            wrongAnswer = false;

            if (answer.equals("a")){
                System.out.printf("Vastus on %d\n",sum(a, b));
            }
            else if(answer.equals("b")){
                System.out.printf("Vastus on %d\n", sub(a, b));
            }
            else if(answer.equals("c")){
                System.out.printf("Vastus on %f\n", mult(a, b));
            }
            else if (answer.equals("d")){
                System.out.printf("Vastus on %.2f\n", div(a, b));
            }
            else {
                System.out.println("Selline tehe puudub!");
                System.out.println();
                wrongAnswer = true;

            } while (wrongAnswer);
                System.out.println("Kas tahad jätkata? jah/ei");


        } while (scanner.nextLine().equalsIgnoreCase("jah")); {
            }
    }

    static int sub(int a, int b) {
        int sub = a - b;
        return sub;
    }
    static int sum(int a, int b) {
        int sum = a + b;
        return sum;
    }
    static double div(double a, double b) {
        double div = a / b;
        return div;
    }
    static double mult(double a, double b) {
        double mult = a * b;
        return mult;
    }
}
