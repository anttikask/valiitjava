package it.vali;

import java.util.ArrayList;
import java.util.List;

enum Fuel {
    GAS, DIESEL, HYBRID, ELECTRIC, PETROL
}

public class Car {

    private String make;
    private String model;
    private int year;
    private Fuel fuel;
    private boolean isUsed;

    private Person driver;
    private Person owner;
    private Person passanger;

    private boolean isEngineRunning;
    private int speed;
    private int maxSpeed;

    private int maxPassangers;
    private List<Person> passangers = new ArrayList<Person>();


    // Lisa autole max reisijate arv.
    // Lisa autole võimalus hoida reisijaid.
    // Lisa meetodid reisijate lisamiseks ja eemdaldamiseks autost.
    // Kontrolli ka, et ei lisaks rohkem reisijaid kui mahub.

    public int getMaxPassengers() {
        return maxPassangers;
    }


    public void setMaxPassangers(int maxPassangers){
        if (maxPassangers > 5){
            System.out.println("Liiga palju reisijaid");
        } else {
            this.maxPassangers = maxPassangers;
        }
    }

    public void addPassanger (Person passanger){
        if(passangers.size() <maxPassangers) {
            if (passangers.contains(passanger)) {
                System.out.printf("Autos juba on reisija %s", passanger.getFirstName());
            }
            else {
                passangers.add(passanger);
                System.out.printf("Autosse lisati reisija %s", passanger.getFirstName());
            }
        }
        else {
            System.out.println("Autosse ei mahu enam reisijaid");
        }
    }

    public void removePassanger (Person passanger){
        if(passangers.indexOf(passanger) != -1) {
            passangers.remove(passanger);
            System.out.printf("Autost eemaldati reisija %s", passanger.getFirstName());
        }
        else {
            System.out.println("Sellist reisijat autos pole");
        }
    }

    public void showPassangers() {

        // For each tsükkel - iga elemendi kohta listis passangers tekita objekt passanger
        // 1. kordus - Person passanger on esimene reisija
        // 2. kordus - Person passanger on teine reisija
        // 3. kordus - Person passanger on kolmas reisija
        System.out.println("Autos on järgnevad reisijad:");
        for(Person passenger : passangers) {
            System.out.println(passenger.getFirstName());
        }
    }

    public Person getDriver() {
        return driver;
    }

    public void setDriver(Person driver) {
        this.driver = driver;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    // Constructor - on eriline meetod, mis käivitatakse klassist objekti loomisel.
    public Car(){
        System.out.println("Loodi auto objekt");
        // Igale Car objektile deklareeritakse  muutujad kindla väärtusega
        maxSpeed = 200;
        isUsed = true;
        fuel = Fuel.PETROL;
    }

    // Constructor overloading
    public Car(String make, String model, int year, int maxSpeed){
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;
    }

    public Car(Fuel fuel, boolean isUsed){
        this.fuel = fuel;
        this.isUsed = isUsed;
    }

    // 4. kodune ülesanne - Loo mõni auto objekt, kellel on määratud kasutaja ja omanik.

    public void startEngine() {
        if (!(isEngineRunning)) {
            isEngineRunning = true;
            System.out.println("Mootor käivitus");
        }
        else {
            System.out.println("Mootor juba töötab");
        }
    }

    public void stopEngine() {
        if (isEngineRunning){
            isEngineRunning = false;
            System.out.println("Mootor seiskus");
        }
        else {
            System.out.println("Mootor ei töötanudki");
        }
    }

    public String getMake() {
        return make;
    }


    public void accelerate(int targetSpeed) {
        if (!(isEngineRunning)){
            System.out.println("Mootor ei tööta, ei saa kiirendada");
        } else if (targetSpeed > maxSpeed){
            System.out.println("Auto nii kiiresti ei sõida");
            System.out.printf("Maksimum kiirus on %d%n", maxSpeed);
        } else if (targetSpeed < 0){
            System.out.println("Valitud kiirus ei saa olla negatiivne");
        } else if (targetSpeed < speed) {
            System.out.println("Auto ei taha kiirendada väiksemale kiirusele");
        } else {
            speed = targetSpeed;
            System.out.printf("Auto kiirendas kuni kiiruseni %d%n", targetSpeed);
        }
    }

    // 1. kodune ül. - slowDown()
    public void slowDown (int targetSpeed){
        if (targetSpeed > 0){
            System.out.println("Pead aeglustama, auto sõidab liiga kiiresti");
        } else if (targetSpeed < 0){
            System.out.println("Auto soovitud kiirus ei saa olla negatiivne");
        } else {
            speed = targetSpeed;
            isEngineRunning = true;
            stopEngine();
            System.out.printf("Auto kiirus on %d\n", speed);
        }
    }

    // 2. kodune ül. - park()
    public void park (int targetSpeed, boolean isEngineRunning) {
            slowDown(0);
            stopEngine();
            }
    }


