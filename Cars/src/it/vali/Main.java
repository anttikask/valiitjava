package it.vali;

import java.util.List;

public class Main {

    public static void main(String[] args) {
	    Car bmw = new Car();
	    Car fiat = new Car();
	    Car mercedes = new Car();
	    Car opel = new Car("Opel", "Vectra", 1999, 205);

	    Person person = new Person("Antti", "Kask", Gender.MALE, 31);
		Person newPerson = new Person("Suva", "Tüüp", Gender.MALE, 60);
		Person newPerson2 = new Person("Mari", "Rebane", Gender.FEMALE, 19);
		Person newPerson3 = new Person("Mart", "Helme", Gender.NOTSPECIFIED, 90);

	    // Kodune ülesanne 1. - slowDown()
	    opel.slowDown(5);

		System.out.println();

	    // Kodune ülesanne 2. - park()
		opel.park(0, true);

		opel.setMaxPassangers(4);
        opel.addPassanger(newPerson3);
        opel.addPassanger(newPerson2);

        opel.showPassangers();
        opel.removePassanger(newPerson2);

        opel.addPassanger(newPerson3);

        Person juku = new Person ("juku", "juurikas", Gender.MALE, 13);
        Person malle = new Person ("malle", "maasikas", Gender.FEMALE, 30);

        System.out.println(juku);









    }
}
