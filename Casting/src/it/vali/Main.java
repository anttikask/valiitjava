package it.vali;

public class Main {

    public static void main(String[] args) {
	//Casting on teisendamine ühest arvutüübist teise.
        byte a = 3;
        //Kui üks numbritüüp mahub teise sisse, siis toimub automaatne teisendamine ("implicit casting").
        short b = a;

        System.out.println(a);

        //Kui üks numbritüüp ei pruugi mahtuda teise sisse, siis peab kõigepealt ise veenduma, et see number mahub teise numbri tüüpi ja kui mahub, siis peab ise seda teisendama ("explicit casting)

        short c = 300;
        byte d = (byte) c;

        System.out.println(d);

        long e = 10000000000L;
        int f = (int) e;

        System.out.println(f);

        float h = 123.23424F;
        double i = h;

        float z = (float) i;

        double j = 55.11111111111111;
        float k = (float) j;

        System.out.println(k);

        double l = 12E50; //12 * 10^50
        float m = (float) l;
        System.out.println(m);

        int n = 3;
        double o = n;

        double p = 2.99;
        short q = (short) p;

        System.out.println(q);


        int r = 2;
        double s = 9;
        //int / int = int;
        // int / double = double;
        // double / int = double;
        // double + int = double;
        System.out.println(r / s);

        float t = 12.55555F;
        double u = 23.555555;

        System.out.println(t / u);




    }
}
