package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Sisesta number");

        Scanner scanner = new Scanner(System.in);
        int a = Integer.parseInt(scanner.nextLine());

        //Kontrolli, kas a on võrdne 3-ga
        if (a == 3) {
            System.out.printf("Arv %d on võrdne 3-ga%n", a);
        }
        if (a < 5) {
            System.out.printf("Arv %d on väiksem 5%n", a);
        }
        if (a != 4) {
            System.out.printf("Arv %d ei võrdu 4-ga%n", a);
        }
        if (a > 2) {
            System.out.printf("Arv %d on suurem 2-st%n", a);
        }
        if (a >= 7) {
            System.out.printf("Arv %d on suurem või võrdne 7-st%n", a);
        }

        if (!(a >= 7)) {
            System.out.printf("Arv %d ei ole suurem või võrdne 7-ga%n", a);
        }

        if (a > 2 && a < 8) {
            System.out.printf("Arv %d on suurem 2-st ja väiksem 8-st%n", a);
        }

        if (a < 2 || a > 8) {
            System.out.printf("Arv %d on väiksem 2-st või suurem 8-st%n", a);
        }
        //&& on tehete järjekorras eespool ||
        if ((a > 2 && a < 8) || (a > 5 && a < 8) || (a > 10)) {
            System.out.printf("Arv %d on 2 ja 8 vahel või 5 ja 8 vahel või on suurem 10%n", a);
        }
        //Kui üks tingimustest leitakse, et on vale, siis järgmisi tingimusi enam ei vaadata
        if ((!(a > 4 && a < 6) && (a > 5 && a < 8)) || (a < 0 && !(a > -14))) {
            System.out.printf("Arv %d ei ole 4 ja 6 vahel aga on 5 ja 8 vahel%n", a);
        }
    }
}
