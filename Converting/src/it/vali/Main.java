package it.vali;

import java.util.Locale;
import java.util.Scanner;

import static java.lang.String.*;

public class Main {

    public static void main(String[] args) {
        //Küsi kasutajalt 2 numbrit ja prindi välja nende summa
        Scanner scanner = new Scanner(System.in);

        System.out.println("Sisesta esimene number");

        int a = Integer.parseInt(scanner.nextLine());

        System.out.println("Sisesta teine number");

        int b = Integer.parseInt(scanner.nextLine());

        System.out.println("Nende arvude summa on: " + (a + b));

        //Küsi kasutajatelt 2 reaalarvu ja prindi välja nende jagatis

        System.out.println("Sisesta esimene reaalarv");

        double c = Double.parseDouble(scanner.next());

        System.out.println("Sisesta teine reaalarv");

        double d = Double.parseDouble(scanner.next());
        //Lisades .2 f-i ette, saame kahe koma koha võrra jätta lõpliku väärtuse
        System.out.printf("Arvu %.2f ja arvu %.2f jagatis on %.2f\n", c, d, c / d);
    }
}
