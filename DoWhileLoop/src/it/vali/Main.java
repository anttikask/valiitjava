package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// while tsükli näide:

//        System.out.println("Kas tahad jätkata? (jah/ei)");
//        //Jätkame seni kuni kasutaja kirjutab "ei"
//        Scanner scanner = new Scanner(System.in);
//        String answer = scanner.nextLine();
//
//        while(!answer.equals("ei")){
//            System.out.println("Oled kindel? jah/ei");
//            answer = scanner.nextLine();
//        }
//        System.out.println("Väga hea");



        //do while tsükkel on nagu while tsükkel, ainult et kontroll tehakse peale esimest kordust.
        //Do loogsulgude sees olev tehakse vähemalt 1 korra niikuinii
        Scanner scanner = new Scanner(System.in);
        String answer; //iga muutuja, mille me deklareerime kehtib ainult seda ümbritsevate loogsulgude sees

        do {
            System.out.println("Kas tahad jätkata? jah/ei");
            answer = scanner.nextLine();
        }
        while(!(answer.equals("ei"))); {
        }
        System.out.println("ahah");
    }
}
