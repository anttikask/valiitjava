package it.vali;

public class Main {

    public static void main(String[] args) {
        // Muutujate vaikeväärtused, kui me pole neile väärtuseid andnud
        // int 0,
        // boolean false,
        // double 0.0,
        // float 0.0f,
        // String null,
        // Objektide (Monitor, FileWriter) null,
        // Massiivid int[] arvud on null

	    Monitor firstMonitor = new Monitor();
	    firstMonitor.setDiagonal(23);
        System.out.println(firstMonitor.getDiagonal());

        System.out.println(firstMonitor.getYear());
        System.out.println(firstMonitor.getDiagonal());

        firstMonitor.setManufacturer("Huawei");
        System.out.println(firstMonitor.getManufacturer());
    }
}
