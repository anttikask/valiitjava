package it.vali;

import java.util.List;

// 5. Defineeri klass Language, sellel klassil getLanguageName, setLanguageName ning list riikide nimedega,
//    kus seda keelt räägitakse. Kirjuta üle selle klassi meetod toString() nii, et see tagastab riikide nimekirja eraldades komaga.
//    Tekita antud klassist 1 objekt ühe vabalt valitud keele andmetega ning prindi välja selle objekti toString() meetodi sisu.

public class Language {
    private String languageName;
    private List<String> countryNames;

    public List<String> getCountryNames() {
        return countryNames;
    }

    public void setCountryNames(List<String> countryNames) {
        this.countryNames = countryNames;
    }


    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    @Override
    public String toString() {
        return String.join(", ", countryNames);
    }
}
