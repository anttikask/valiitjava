package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {




    public static void main(String[] args) {

        // 1. ül lahendus
        double radius = 10.5;
        System.out.printf("Ringi pindala on: %s%n", areaOfCircle(radius));

        // 2. ül lahendus
        String a = "Tere";
        String b = "Koer";
        System.out.println(compareStrings(a, b));

        // 3. ül lahendus
        int[] numbers = new int[]{1, 4, 2, 3} ;

        // 4. ül lahendus

        List<Integer> years = leapYearsInCentury(1400);

        for (int year : years) {
            System.out.println(year);
        }

        // 5. ül lahendus

        Language language = new Language();
        language.setLanguageName("English");

        List<String> countryNames = new ArrayList<>();
        countryNames.add("England");
        countryNames.add("India");
        countryNames.add("Australia");
        countryNames.add("USA");


        language.setCountryNames(countryNames);

        System.out.println(language.toString());


    }

    // 1. Arvuta ringi pindala, kui on teada raadius. Prindi pindala välja ekraanile.
    static double areaOfCircle(double radius) {
        return radius * Math.pow(Math.PI, 2);

    }

    // 2. Kirjuta meetod, mis tagastab boolean tüüpi väärtuse ja mille sisendparameetriteks on kaks stringi. Meetod
    //    tagastab kas tõene või vale selle kohta, kas stringid on võrdsed.

    static boolean compareStrings(String a, String b){
        if (a.equals(b)) {
            return true;
        }
        return false;
    }

    // 3. Kirjuta meetod, mille sisendparameetriks on täisarvude massiiv ja mis tagastab stringide massiivi. Iga sisend-massiivi
    //    elemendi kohta olgu tagastatavas massiivis samapalju a tähti. (3, 6, 7: "aaa", "aaaaaa", "aaaaaaa").

    static String[] convertArrays (int[] numbers) {
        String[] words = new String[numbers.length];
        for (int i = 0; i < words.length ; i++) {
            words[i] = generateAString(numbers[i]);
            System.out.println(words[i]);
        }
        return words;
    }

    static String generateAString (int count) {
        String word = "";

        for (int i = 0; i < count ; i++) {
            word = word + "a"; //ehk word += "a";
        }
        return word;
    }

    // 4. Kirjuta meetod, mis võtab sisendparameetrina aastaarvu (sisestada saab ainult aastaid vahemikus 500 - 2019.)
    //    ja tagastab kõik sellel sajandil esinenud liigaastad.

    static List<Integer> leapYearsInCentury(int year){
        List <Integer> years = new ArrayList<>();

        if (year < 500 || year > 2019) {
            System.out.println("Aasta peab olema vahemikus 500 - 2019");
            return years;
        }

        int centuryStart = year/100 * 100;
        int centuryEnd = centuryStart + 99;
        int leapYear = 2020;

        for (int i = 2020; i >= centuryStart ; i-=4) {
            if(i <= centuryEnd && i % 4 == 0){
                years.add(i);
            }
        }
        return years;

    }
}
