package it.vali;
//Import tähendab, et antud klassile Main lisatakse ligipääs Java class library java.util paiknevale klassile Scanner
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //Loome Scanner tüübist objekti nimega scanner, mille kaudu saab kasutaja sisendit lugeda
	    Scanner firstName = new Scanner(System.in);

	    System.out.println("Mis Su eesnimi on?");
        String nimi = firstName.nextLine();

	    Scanner lastName  = new Scanner(System.in);

	    System.out.println("Mis Su perenimi on?");
        String perenimi = lastName.nextLine();

        Scanner auto = new Scanner(System.in);

        System.out.println("Mis autoga sõidad?");
        String autonimi = auto.nextLine();

        System.out.println("Tere, " + nimi + " " + perenimi + ", lahe auto see" + " " + autonimi);

        //%s - stringi placeholder
        //printf annab võimsluse stringi formatida ehk kasutada placeholdereid
        System.out.printf("Tere, %s %s, lahe auto see %s \n", nimi, perenimi, autonimi);
        //String builder laseb olemasolevatest stringitest kokku ühe stringi ehitada .toString() meetodiga
        StringBuilder builder = new StringBuilder();
        builder.append("Tere, ");
        builder.append(nimi);
        builder.append(" ");
        builder.append(perenimi);
        builder.append(", lahe auto see ");
        builder.append(autonimi);
        //prinditakse otse välja kokkupandud stringid .toString meetodiga - StringBuilderi objekti nimeks sai builder
        System.out.println(builder.toString());

        //Nii system.out.printf kui ka String.format kasutavad enda siseselt StringBuilderit
        String text = String.format("Tere, %s %s, lahe auto see %s \n", nimi, perenimi, autonimi);
        System.out.println(text);






    }
}
