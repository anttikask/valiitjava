package it.vali;

public class Main {

    public static void main(String[] args) {
	    //Tekstitüüpi (String variable) muutja deklareerimine/defineerimine, mille nimeks paneme "name" ja väärtuseks "Antti"
        String name = "Antti";
        String lastName = "Kask";
        System.out.println("Hello, " + name + " " + lastName);
    }
}
