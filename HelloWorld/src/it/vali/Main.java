// public - tähenda, et klass,
// meetod või muutuja on avalikult nähtav/ligipääsetav
// class - javas üksus, üldiselt eraldi fail, mis sisaldab/grupeerib mingit funktsionaalsust
// HelloWorld - klassi nimi, mis on ka faili nimi.
// static - meetodi ees tähendab, et seda meetodit saab välja kutsuda ilma klassist objekti loomata
// void - meetod ei tagasta midagi.
// 			meetodile on võimalik kaasa anda parameetrid, mis pannakse sulgude sisse, eraldades komaga.
// String[] - tähistab stringi massiivi.
// args - massivi nimi, sisaldab käsurealt kaasa pandud parameetreid.
// System.out.println - java meetod, millega saab printida välja rida teksti
// 									see, mis pannakse sulgudesse, prinditakse välja ja tehakse reavahetus.
package it.vali;

public class Main {

    public static void main(String[] args) {
	    System.out.println("Hello, World!");
    }
}
