package it.vali;

public class Main {

    public static void main(String[] args) {

    	//Koostame kassi objektid
    	Cat angora = new Cat();
	    angora.setName("Miisu");
	    angora.setAge(10);
	    angora.setBreed("Angora");
	    angora.setWeight(2.34);

	    angora.printInfo();
	    angora.eat();

	    Cat persian = new Cat();
		persian.setName("Liisu");
		persian.setAge(1);
		persian.setBreed("Persian");
		persian.setWeight(3.11);

		persian.printInfo();
		persian.eat();

		// Koostame koera objekti
		Dog tax = new Dog();
		tax.setName("Muki");
		tax.setAge(3);
		tax.setBreed("Taks");
		tax.setWeight(3.22);

		tax.printInfo();
		tax.eat();

		persian.catchMouse();
		tax.playWithCat(persian);

		// Lisa pärinvusaheldast puuduvad klassid
		// Igale klassile lisa 1 muutuja ja 1 meetod


    }
}
