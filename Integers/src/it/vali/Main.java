package it.vali;

public class Main {

    public static void main(String[] args) {
	    String word;
	    word = "Kala";

	    int number;
	    number = 3;

	    int secondNumber = -7;

	    int korrutis = number * secondNumber;

	    System.out.println(number);
	    System.out.println(secondNumber);
	    System.out.println(number + secondNumber);

	    //%n tekitab platvormi spetsiifilise reavahetuse (Windows \r\n, Linux/Mac \n - selle asemel saab kasutada ka funktsiooni System.lineSeparator()
	    System.out.printf("Arvude %d ja %d korrutis on %d%n", number, secondNumber, korrutis);
		System.out.printf("Arvude %d ja %d korrutis on %d%n", number, secondNumber, number * secondNumber);

		double a = 3.0, b = 5;

		//String + number on alati String. Stringi liitmisel numbriga teisendatakse number stringiks ja liidetakse kui liitsõńa. Et probleemi vältida, tuleks sulge kasutada.
		System.out.println("Arvude summa on " + (a + b));

		System.out.println("Arvude a ja b jagatis on " + (a / b));
		System.out.println("Arvude b ja a jagatis on " + (b / a));

		//Kahe täisarvu jagamisel on tulemus jagatise täisosa ehk kõik peale koma süüakse ära

		int maxint = 2147483647;
		int c = maxint + 1;
		int d = maxint + 2;
		System.out.println(c);
		System.out.println(d);

		short f = 32199;
		//Long väärtusele tuleks l-täht lõppu lisada
		long g = 8000000000l;
		long h = 234;

		System.out.println(g + h);
    }
}
