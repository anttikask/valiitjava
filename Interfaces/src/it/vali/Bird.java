package it.vali;

// Bird klass kasutab/implementeerib liidest Flyer
public class Bird implements Flyer {
    @Override
    public void fly() {
        System.out.println("Lind lendab");
    }

    private void jumpUp() {
        System.out.println("Lind hüppas õhku");
    }

}
