package it.vali;

public class Car extends Vehicle implements Driver {
    private int speed = 100;
    private int time = 5;
    private int maxDistance;
    private String make;

    public String getMake() {
        return make;
    }

    public void setMake() {
        this.make = make;
    }


    public Car() {
        this.maxDistance = 600;
    }

    public Car(int maxDistance){
        this.maxDistance = maxDistance;
    }

    @Override
    public String toString() {

        return super.toString();
    }

    @Override
    public void drive() {
        System.out.println("Auto sõidab");
    }

    @Override
    public void stopDriving(int afterDistance) {
        if (afterDistance > 200){
            System.out.println("Peatu, sul on vaja puhata");
        }
    }

    @Override
    public int getMaxDistance() {
        maxDistance = speed * time;
        return maxDistance;
    }

}
