package it.vali;

public interface Driver {
    void drive();
    void stopDriving(int afterDistance);
    int getMaxDistance();
}
