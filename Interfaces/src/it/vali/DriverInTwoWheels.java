package it.vali;

public interface DriverInTwoWheels extends Driver {
    void driveInRearWheel();
}
