package it.vali;

// Interface ehk liides sunnib seda implementeerivat (nt. Plane või Bird)
// klassi omama liideses olevaid meetodeid.(sama tagastuse tüübiga ja sama parameetrite kombinatsiooniga)

// Iga klass, mis interface'i kasutab, määrab ise ära meetodi sisu. Interface'i sees meetodeid ei määrata
public interface Flyer {
    void fly();
}
