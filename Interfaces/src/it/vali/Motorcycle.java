package it.vali;

public class Motorcycle extends Vehicle implements DriverInTwoWheels {
    @Override
    public void drive() {
        System.out.println("Mootorratas sõidab");
    }

    @Override
    public void stopDriving(int afterDistance) {
        System.out.printf("Mootorratas lõpetab sõitmise %d km pärast", afterDistance);
    }

    @Override
    public int getMaxDistance() {
        return 0;
    }

    @Override
    public void driveInRearWheel() {

    }
}
