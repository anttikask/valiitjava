package it.vali;

// Plane klass pärineb klassist Vehicle ja implementeerib liidese Flyer
public class Plane extends Vehicle implements Flyer, Driver {

    private int maxDistance;

    @Override
    public void fly() {
        doCheckList();
        startEngine();
        System.out.println("Lennuk lendab");
    }

    @Override
    public void drive() {
        System.out.println("Lennuk sõidab");
    }

    @Override
    public void stopDriving(int afterDistance) {

    }

    @Override
    public int getMaxDistance() {
        return maxDistance;
    }

    private void doCheckList() {
        System.out.println("Tehakse checklist");
    }

    private void startEngine() {
        System.out.println("Mootor käivitus");
    }
}
