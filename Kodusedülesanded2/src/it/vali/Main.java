package it.vali;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // 1. Koosta täisarvude massiiv 10 arvust ning seejärel kirjuta faili kõik suuremad arvud kui 2
        int[] integerArray = new int[]{23, 12, 65, 1, 2, 34, 54, 12, 31, 12};

        try {
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\GitHub\\valiitjava\\Kodusedülesanded2\\täisarvudeMassiiv.txt");
            for (int i = 0; i < integerArray.length; i++) {
                if (integerArray[i] > 2) {
                    fileWriter.append(integerArray[i] + "\r\n");
                }
            }
            fileWriter.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }

        // 2. Küsi kasutajalt kaks arvu. Küsi seni kuni mõlemad on korrektsed (on numbriks teisendatavad) ning nende summa ei ole paaris arv.

        // 3. Küsi kasutajalt mitu arvu ta tahab sisestada, seejärel küsi ühe kaupa kasutajalt need arvud ning kirjuta nende arvude summa faili, nii et see lisatakse alati juurde (Append)
        // Tulemus on iga programmi käivitamisel järel on failis kõik eelnevad summad kirjas.
            System.out.println("Mitu arvu soovid sisestada? Kirjuta number 2-4.");
            int ccount = Integer.parseInt(scanner.nextLine());
            int sum = 0;

            for (int i = 0; i < ccount ; i++) {
                System.out.printf("Sisesta arv number %d\n", i + 1);
                int number = Integer.parseInt(scanner.nextLine());
                sum += number;
            }
            try{
                FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\GitHub\\valiitjava\\Kodusedülesanded2\\arvudeSummad.txt", true);
                fileWriter.append(sum + System.lineSeparator());
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
}

