package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Farm implements LivingPlace {
    private List<FarmAnimal> animals = new ArrayList<FarmAnimal>();
    // Siin hoitakse infot, palju meil igat looma farmis on
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();
    // Siin hoitakse infot, palju meile igat looma farmi mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    public Farm() {
        maxAnimalCounts.put("Pig", 3);
        maxAnimalCounts.put("Cow", 3);
        maxAnimalCounts.put("Sheep", 4);

    }
    @Override
    public void addAnimal(Animal animal) {
        // Kontrollime, kas animal on tüübist FarmAnimal või pärineb sellest tüübist
        if (!FarmAnimal.class.isInstance(animal)){
            System.out.println("Farmis saavad elada ainult farmi loomad");
            return;
        }

        String animalType = animal.getClass().getSimpleName();

        if (!maxAnimalCounts.containsKey(animalType)) {
            System.out.println("Farmis selliseid loomi pole");
            return;
        }

        if (animalCounts.containsKey(animalType)) {
            int animalCount = animalCounts.get(animalType);
            int maxAnimalCount = maxAnimalCounts.get(animalType);
                if (animalCount >= maxAnimalCount) {
                    System.out.println("Farmis on sellele loomale kõik kohad juba täis");
                    return;
                }
            animalCounts.put(animalType, animalCounts.get(animalType) + 1);
        // Sellist looma veel ei ole farmis
        // Kindlasti on sellele loomale koht olemas
        } else {

            animalCounts.put(animalType, 1);
        }
        animals.add((FarmAnimal) animal);
        System.out.printf("Farmi lisati loom %s%n", animalType);
    }

    // Tee meetod, mis prindib välja kõik farmis elavad loomad ja mitu neid on
    @Override
    public void printAnimalCounts() {
        //Entryga tuleb teha see
    }
    // Tee meetod, mis eemaldab farmist looma
    @Override
    public void removeAnimal(String animaltype) {

        boolean animalFound = false;

        for (FarmAnimal animal: animals) {
            if (animal.getClass().getSimpleName().equals(animaltype)) {
                animals.remove(animal);
                System.out.printf("Farmist eemdaldati loom %s%n", animaltype);

                // Kui see oli viimane loom, siis eemalda see rida animalCounts mapist
                // muul juhul vähenda animalCounts mapis seda kogust.
                if (animalCounts.get(animaltype) == 1) {
                    animalCounts.remove(animaltype);
                } else {
                    animalCounts.put(animaltype, animalCounts.get(animaltype) - 1);
                }
                animalFound = true;
                break;
                // Täienda meetodit nii, et kui ei leitud ühtegi sellest tüübist looma siis prindi välja "Farmis selline loom puudub"
            }
        }
        if (!animalFound){
            System.out.println("Farmis antud loom puudub");
        }
    }
}
