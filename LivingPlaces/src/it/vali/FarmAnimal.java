package it.vali;

public class FarmAnimal extends DomesticAnimal {

    private double foodConsumedPerYear;

    public double getFoodConsumedPerYear() {
        return foodConsumedPerYear;
    }

    public void setFoodConsumedPerYear(double foodConsumedPerYear) {
        this.foodConsumedPerYear = foodConsumedPerYear;
    }

    public void giveBirth(){
        System.out.println("Hakkan sünnitama");
    }
}
