package it.vali;

public interface LivingPlace {
    void removeAnimal(String animaltype);
    void printAnimalCounts();
    void addAnimal(Animal animal);
}
