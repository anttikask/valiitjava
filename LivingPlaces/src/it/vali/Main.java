package it.vali;

public class Main {

    public static void main(String[] args) {
	    LivingPlace livingPlace = new Farm();

	    Pig pig = new Pig();
	    pig.setName("Kalle");

        livingPlace.addAnimal(new Cat());
        livingPlace.addAnimal(new Horse());
        livingPlace.addAnimal(new Pig());
        livingPlace.addAnimal(new Pig());
        livingPlace.addAnimal(new Pig());
        livingPlace.addAnimal(new Pig());
        Cow cow = new Cow();
        cow.setName("Priit");

        System.out.println();

        livingPlace.printAnimalCounts();
        livingPlace.removeAnimal("Pig");
        livingPlace.printAnimalCounts();
        livingPlace.removeAnimal("Pig");
        livingPlace.printAnimalCounts();

        livingPlace.removeAnimal("Põder");

        // Mõelge ja täiendage Zoo ja Forest klasse, nii, et neile oleks nende kolme meetodi sisu.

    }
}
