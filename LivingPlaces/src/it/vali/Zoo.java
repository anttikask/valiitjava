package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Zoo implements LivingPlace {
    private List<Animal> animals = new ArrayList<Animal>();
    // Siin hoitakse infot, palju meil igat looma farmis on
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();
    // Siin hoitakse infot, palju meile igat looma farmi mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    public Zoo() {
        maxAnimalCounts.put("Lion", 2);
        maxAnimalCounts.put("Fox", 3);


    }

    @Override
    public void removeAnimal(String animaltype) {


    }

    @Override
    public void printAnimalCounts() {

    }

    @Override
    public void addAnimal(Animal animal) {
        // Kontrollime, kas animal on tüübist Pet või pärineb sellest tüübist
        if (!Pet.class.isInstance(animal)) {
            System.out.println("Lemmikloomi loomaaeda vastu ei võeta");
            return;
        }

        // getSimpleName annab it.vali.Cat'i asemel ainult klassi nime ehk Cat'i
        String animalType = animal.getClass().getSimpleName();

        if (!maxAnimalCounts.containsKey(animalType)) {
            System.out.println("Farmis selliseid loomi pole");
            return;
        }

        if (animalCounts.containsKey(animalType)) {
            int animalCount = animalCounts.get(animalType);
            int maxAnimalCount = maxAnimalCounts.get(animalType);
            if (animalCount >= maxAnimalCount) {
                System.out.println("Farmis on sellele loomale kõik kohad juba täis");
                return;
            }
            animalCounts.put(animalType, animalCounts.get(animalType) + 1);
            // Sellist looma veel ei ole farmis
            // Kindlasti on sellele loomale koht olemas
        } else {

            animalCounts.put(animalType, 1);
        }
        animals.add((FarmAnimal) animal);
        System.out.printf("Farmi lisati loom %s%n", animalType);
    }
}
