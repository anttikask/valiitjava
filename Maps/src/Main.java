import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<String, String>();

        // Sõnaraamat:
        // key => value
        // Maja => House
        // Isa => Father
        // Kuu => Moon

        map.put("Maja", "House");
        map.put("Isa", "Father");
        map.put("Kuu", "Moon");
        map.put("Sinine", "Blue");

        // Oletame, et tahan teada, mis on inglise keeles Kuu

        String translation = map.get("Kuu");
        System.out.println(translation);

        Map<String, String> id = new HashMap<String, String>();

        id.put("12421341234", "Jüri");
        id.put("24453564256", "Mari");
        id.put("23434531234", "Muki");
        id.put("3462453452", "Karl");

        System.out.println(id.get("12421341234"));

        // Kui kasutada put sama key lisamisel, kirjutatakse value üle

        System.out.println(id.get("23434531234"));
        id.remove("23434531234");
        id.get("23434531234");
        System.out.println(id.get("23434531234"));

        // Loe lauses üle kõik erinevad tähed
        // ning prindi välja iga tähe järel, mitu tükki teda selles lauses oli

        // Char on selline tüüp, kus saab hoida üksikut sümbolit
        char symbolA = 'a';
        char symbolB = 'b';
        char newLine = '\n';

        String sentence = "elas metsas mutionu";

        Map<Character, Integer> letterCounts = new HashMap<Character, Integer>();

        char[] characters = sentence.toCharArray();

        for (int i = 0; i <characters.length ; i++) {
            if(letterCounts.containsKey(characters[i])){
                letterCounts.put(characters[i], letterCounts.get(characters[i]) + 1);
            } else {
                letterCounts.put(characters[i], 1);
            }
        }

        // Map.Entry<Character, Integer> on klass, mis hoiab endas ühte rida map-is
        // ehk siis üht key => value kombinatsiooni.
        for (Map.Entry<Character, Integer> entry : letterCounts.entrySet()) {
            System.out.printf("Tähte %s esines %d korda\n", entry.getKey(), entry.getValue());
        }
    }

    Map<String, String> linkedMap = new LinkedHashMap<String, String>();

    // Sõnaraamat:
    // key => value
    // Maja => House
    // Isa => Father
    // Kuu => Moon


}
