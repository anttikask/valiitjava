package it.vali;

import java.util.Scanner;

public class Main {

    // Meetodid on mingid koodi osad, mis grupeerivad mingit teatud kindlat funktsionaalsust.
    // Kui koodis on korduvaid koodi osasid, võiks mõelda et äkki peaks nende kohta tegema eraldi meetodi.

    public static void main(String[] args) {          // Hakkame main methodi all järjest teisi meetodeid välja kutsuma.
	    printHello();
        printHello(3);
        printText("Kuidas läheb?");
        printText("Väga hästi", 6);
        printText("tere", 1, true);
    }

    // Tekitame main meetodi kõrvale uue meetodi, mis prindib ekraanile Hello:
    private static void printHello() {              // meetod on private, sest kasutame seda klassi sees ja pole otsest põhjust seda avalikuks teha
        System.out.println("Hello");
    }
    // Lisame meetodi, mis prindib Hello etteantud arv kordi:
    static void printHello(int howManyTimes) {     // vaikimisi on meetod private. printHello sees on parameeter howManyTimes, mille datatüüp on
        for (int i = 0; i <=howManyTimes ; i++) {  // For tsükkel, mis toimub howManyTimes argumendi suuruse jagu.
            System.out.println("Hello");
        }
    }

    // Lisame meetodi, mis prindib ette antud teksti välja printText
    static void printText(String text) {
        System.out.println(text);
    }

    // Lisame meetodi, mis prindib ette antud teksti välja ette antud arv kordi
    static void printText(String text, int howManyTimes) {
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println(text);
        }
    }
    // Method OVERLOADING - meil on mitu meetodit sama nimega, aga erineva parameetrite kombinatsiooniga.


    // Lisame meetodi, mis prindib ette antud teksti välja ette antud arv kordi, lisaks saab öelda, kas tahame teha kõik tähed suurteks või mitte
    static void printText(String text, int howManyTimes, boolean toUpperCase) {
        for (int i = 0; i < howManyTimes; i++) {
            if(toUpperCase) {
                System.out.println(text.toUpperCase());
            }
            else {
                System.out.println(text);
            }
        }
    }
}