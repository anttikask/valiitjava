package it.vali;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        //Deklareerime uue massiivi "numbrid", elementide arvu me eraldi ei ütle,
        //aga anname infot mis on elementide väärtused ja sealt võetakse massiivi pikkus
        int[] numbers = new int[]{-2, 4, 10, 18, 14, 8};
        //Otsime massiivis kõige suuremat numbrit
        int max = numbers[0];
        //For tsükliga hakkame "läbi käima" järgnevaid elemente ja hakkame neid omavahel võrlema ja proovime leida suurima numbri
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] > max) {
                max = numbers[i];
            }
        }
        System.out.println(max);
        //Kontrollime, mis on miinimumväärtus massiivis for tsükliga.
        //Esialgu seame kõige esimese elemendi eeldatavalt kõige väiksemaks väärtuseks ning seejärel
        //hakkame kontrollima for tsükliga seda elementidega võrdlemisel
        int min = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] < min) {
                min = numbers[i];
            }
        }
        System.out.println(min);

        System.out.println();
        //Leia suurim paaritu arv ja mitmes number see nimekirjas on:
        max = Integer.MIN_VALUE;
        int indexOfmaxPaaritu = 1;
        boolean oddNumbersFound = false;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > max && numbers[i] % 2 != 0) {
                max = numbers[i];
                indexOfmaxPaaritu = i + 1;
                oddNumbersFound = true;
            }
        }
        if (oddNumbersFound) {
            //Leiame suurima paaritu arvu, prindime selle välja ja ültme, mis kohas see massiivis asub
            System.out.printf("Suurim paaritu arv on %d ja ta asub %d. positsioonil", max, indexOfmaxPaaritu);
        } else {
            System.out.println("Paaritud arvud puuduvad");
        }
    }
}
