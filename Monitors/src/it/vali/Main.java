package it.vali;

public class Main {

    public static void main(String[] args) {
	    // Koostame kaks uut Monitori objekti firstMonitor ja secondMonitor
        Monitor firstMonitor = new Monitor();
        Monitor secondMonitor = new Monitor();
        Monitor thirdMonitor = new Monitor();

        // Anname Monitori objekti tootja väärtuse "Philips
        firstMonitor.manufacturer = "Philips";
        // Kasutame Colori enumit andes firstMonitori objektile värvi White
        firstMonitor.color = Color.WHITE;
        // Anname diagonaali muutujale väärtuse
        firstMonitor.diagonal = 27;
        // Anname screentype muutujale väärtuse
        firstMonitor.screenType = ScreenType.AMOLED;

        // Anname teise monitori objekti muutujatele samuti väärtused
        secondMonitor.manufacturer = "LG";
        secondMonitor.color = Color.BLACK;
        secondMonitor.diagonal = 24;
        secondMonitor.screenType = ScreenType.LCD;

        thirdMonitor.manufacturer = "Samsung";
        thirdMonitor.color = Color.WHITE;
        thirdMonitor.diagonal = 32;
        thirdMonitor.screenType = ScreenType.OLED;

        // Lisa massiivi 3 monitori
        // Prindi välja kõigi monitoride tootja, mille diagonaal on suurem, kui 25 tolli
        Monitor[] monitors = new Monitor[3];
        monitors[0] = firstMonitor;
        monitors[1] = secondMonitor;
        monitors[2] = thirdMonitor;


        for (int i = 0; i < monitors.length ; i++) {
            if (monitors[i].diagonal > 25){
                System.out.println(monitors[i].manufacturer);
            }
        }

        // Leia monitori värv kõige suuremal monitoril
        double maxDiagonal = monitors[0].diagonal;
        for (int i = 0; i < monitors.length; i++) {
            if (monitors[i].diagonal > maxDiagonal) {
                maxDiagonal = monitors[i].diagonal;
                System.out.println(monitors[i].color);
            }
        }
        // Eelmise ülesande parem lahendus
        Monitor maxSizeMonitor = monitors[0];
        for (int i = 0; i < monitors.length ; i++) {
            if ((monitors[i].diagonal > maxSizeMonitor.diagonal)){
                maxSizeMonitor = monitors[i];
            }
        }
        // Kutsume monitori klassi meetodi printInfo, et ekraanile printida monitori info
        maxSizeMonitor.printInfo();
        firstMonitor.printInfo();

        // Kutsume välja diagonalToCm meetodi:
        System.out.printf("Monitori diagonaal cm-tes on %.2f%n", maxSizeMonitor.diagonalToCm());
     }
}

