package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //Küsitakse külastaja nime
        //kui nimi on listis, siis öeldakse kasutajale: "Tere tulemast, ..."
        //ja küsitakse külastaja vanust.
        //Kui kasutaja on alaealine, siis teavitatakse teda, et sisse ei saa
        //Muul juhul öeldakse: Tere tulemast klubisse

        //Kui kasutaja ei olnud listis, küsitakse kasutajalt ka tema perekonnanime
        //Kui perekonnanimi on listis, siis öeldakse Tere tulemast, ees + perenimi
        //Muul juhul öeldakse: "ma ei tunne snid".

        String firstName = "Antti";
        String lastName = "Kask";

        Scanner scanner = new Scanner(System.in);

        System.out.println("Mis Su eesnimi on?");
        String eesNimi = scanner.nextLine();

        if (eesNimi.toLowerCase().equals(firstName.toLowerCase())) {
            System.out.println("Tere " + eesNimi.toUpperCase());
            System.out.println("Kui vana Sa oled " + eesNimi.toUpperCase());
            String vanus = scanner.nextLine();
            int age = Integer.parseInt(vanus);

            if (age < 18) {
                System.out.println("Te ei tohi sisse tulla");
            }
            else {
                System.out.println("Tere tulemast klubisse, " +eesNimi.toUpperCase());
            }
        }
        else{
            System.out.println("Mis on Su perekonnanimi?");
            String pereNimi = scanner.nextLine();
            System.out.println("Kui vana sa oled?");
            String vanus = scanner.nextLine();
            int age = Integer.parseInt(vanus);

            if (pereNimi.toLowerCase().equals(lastName) && age >=18) {
                System.out.println("Tere tulemast," + eesNimi.toUpperCase());
            } else {
                System.out.println("Ma ei tunne sind");
            }
        }

    }
}