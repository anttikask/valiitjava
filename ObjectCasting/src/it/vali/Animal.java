package it.vali;

public class Animal {
    private String name;
    private String breed;
    private int age;
    // protected annab ligipääsu package sees + pärinevad klassid saavad ka selle muutuja endale
    protected double weight;


    public String getName() {
        if (name == null) {
            return "puudub";
        }
        return name;
    }

    public void setName(String name) {

            this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void eat() {
        System.out.println("Söön toitu");
    }

    public void printInfo() {
        System.out.println("Info:");
        System.out.printf("Nimi: %s%n", name);
        System.out.printf("Tõug: %s%n", breed);
        System.out.printf("Vanus: %d%n", age);
        System.out.printf("Kaal: %.2f%n", weight);

    }
}
