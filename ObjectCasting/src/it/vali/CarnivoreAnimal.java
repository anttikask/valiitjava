package it.vali;

public class CarnivoreAnimal extends WildAnimal {

    private int sleepTime;

    public int getSleepTime() {
        return sleepTime;
    }

    public void setSleepTime(int sleepTime) {
        this.sleepTime = sleepTime;
    }

    public void makeTerrifyingNoise(){
        System.out.println("Uurrrr!!!");
    }


}

