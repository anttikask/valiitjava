package it.vali;

public class Cow extends FarmAnimal {

    private int numberOfOffsprings;

    public int getNumberOfOffsprings() {
        return numberOfOffsprings;
    }

    public void setNumberOfOffsprings(int numberOfOffsprings) {
        this.numberOfOffsprings = numberOfOffsprings;
    }

    public int makeSpecificNoise(int numberOfOffsprings){
        return numberOfOffsprings;
    }
}
