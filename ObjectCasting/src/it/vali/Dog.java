package it.vali;

public class Dog extends Pet {

    private boolean hasTail = true;

    public boolean isHasTail() {
        return hasTail;
    }

    public void setHasTail(boolean hasTail) {
        this.hasTail = hasTail;
    }


    public void playWithCat(Cat cat){

        System.out.printf("Mängin kassiga, kelle nimi on %s", cat.getName());
    }

    @Override
    public void eat(){
        System.out.println("Järan konti");
    }

    // Kirjutame Animal klassi getAge() meetdodi Dog klassis üle.
    @Override
    public int getAge(){
        int age = super.getAge();
        if (age == 0){
            return 1;
        }
        return age;
    }

    public double getWeight() {

        return weight;
    }

}
