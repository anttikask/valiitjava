package it.vali;

public class DomesticAnimal extends Animal {

    private boolean isFriendly;

    public boolean isFriendly() {
        return isFriendly;
    }

    public void setFriendly(boolean friendly) {
        isFriendly = friendly;
    }

    public void goInside(){
        System.out.println("Ma lähen sisse ära, talv käes");
    }

}
