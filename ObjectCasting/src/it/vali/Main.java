package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	    // Primitiivmuutujate castimine
        int a = 100;
	    short b = (short) a;

	    // Polymorphism

        // Iga Cat'i võib võtta kui Animal'i
        // Implicit casting
	    Animal animal = new Cat();

	    // Iga Animal ei ole Cat
        // Explicit casting
	    Cat cat = (Cat) animal;

	    // koostame Animal listi nimega animals ning lisame sinna erinevate klasside objekte
        List<Animal> animals = new ArrayList<Animal>();

        Dog dog = new Dog();
        Pet mouse = new Pet();
        Cow lehm = new Cow();

        animals.add(dog);
        animals.add(mouse);
        animals.add(lehm);
        animals.add(new Pig());
        animals.add(new WildAnimal());

        mouse.setName("Priit");
        animals.get(animals.indexOf(dog)).setName("Peeter");

        // Kutsu kõikide listis olevate loomade pinrtInfo meetod välja
        for (Animal animalInList: animals) {
            animalInList.printInfo();
            System.out.println();
        }

        Animal secondAnimal = new Dog();

        ((Dog)secondAnimal).setHasTail(true);
        secondAnimal.printInfo();
    }
}
