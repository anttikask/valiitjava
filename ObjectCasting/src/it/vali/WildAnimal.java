package it.vali;

public class WildAnimal extends Animal {


    private boolean isHunter;

    public boolean isHunter() {
        return isHunter;
    }

    public void setHunter(boolean hunter) {
        isHunter = hunter;
    }

    public void startFindingFood(){
        System.out.println("Leidsin toitu");
    }

    @Override
    public String getName() {
        return "Metsloomal pole nime";
    }

}
