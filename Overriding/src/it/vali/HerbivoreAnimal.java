package it.vali;

public class HerbivoreAnimal extends WildAnimal {

    private String habitat;

    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }

    public void findShelter(String habitat){
        System.out.println("Leidsin pesitsuskoha");
    }
}
