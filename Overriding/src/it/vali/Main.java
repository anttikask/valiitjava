package it.vali;

public class Main {

    public static void main(String[] args) {
	    // Method Overriding ehk meetodi ülekirjutamine. Päritava klassi meetodi sisu kirjutatakse pärinevas klassis üle.

        Dog buldog = new Dog();
        buldog.eat();

        Cat siam = new Cat();
        siam.eat();
        siam.printInfo();

        Pet hiir = new Pet();
        hiir.printInfo();

        System.out.println(hiir.getName());

        // Kirjuta koera getAge üle nii, et kui koeral vanus on 0, siis näitaks 1
        // Metsloomadel printinfo võiks kirjutada Nimi: "Metsloomal pole nime"

        System.out.println();

        WildAnimal metsloom = new WildAnimal();
        metsloom.printInfo();

    }


}
