package it.vali;

public class Pet extends Animal {

    private double heigth = 10.23;

    public double getHeigth() {
        return heigth;
    }

    public void setHeigth(double heigth) {
        this.heigth = heigth;
    }

    public void beFriendly(){
        System.out.println("olen sõbralik");
    }

    @Override
    public void printInfo(){
        super.printInfo();
        System.out.printf("Pikkus on: %s%n", heigth);
    }
}
