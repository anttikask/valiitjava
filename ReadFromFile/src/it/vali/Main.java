package it.vali;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            // Tekitame uue FileReaderi objekti nimega fileReader nin näitame ära selle faili asukoha fileReaderi parameetris
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt");

            // Tekitame uue bufferedReader objekti
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            // readLine loeb iga kord välja kutsudes järgmise rea.
            String line = bufferedReader.readLine();

            // Kui readLine avastab, et järgmist rida tegelikutl ei ole, siis tagastab see meetod null'i.
            while (line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }

            // Alati peab readerid sulgema
            bufferedReader.close();
            fileReader.close();
            // FileReader nõuab FileNotFoundExceptionit
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            // bufferedReader nõuab IOExceptionit, aga üldiselt saab nii FileReaderi kui bufferedReaderi kätte IOExceptioniga
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
