package it.vali;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;

public class Main {

    public static void main(String[] args) {
	// Loe failist input.txt iga teine rida ning kirjuta need read faili output.txt
        try {
            // Notepad kasutab vaikimisi ANSI encoding'ut. Selleks et FileReader oskaks seda korrektselt lugeda (nt. täpitähti), peame talle ette ütlema
            // et loe seda ANSI encoding'us. Cp1252 on java's ANSI encoding.
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt", Charset.forName("Cp1252"));

            //Faili kirjutades on javas default väärtus UTF-8
            FileWriter fileWriter = new FileWriter("c:\\users\\opilane\\Documents\\output.txt", Charset.forName("UTF-8"));
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = bufferedReader.readLine();
            int lineNumber = 1;

            while(line != null) {
                if (lineNumber % 2 == 1) {
                    fileWriter.write(line + System.lineSeparator());
                }
                line = bufferedReader.readLine();
                lineNumber++;
            }
            bufferedReader.close();
            fileReader.close();
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
