package it.vali;

public class Main {

    public static void main(String[] args) {
	    float a = 123.34f;
	    double b = 123.4343;

	    System.out.println("a ja b summa on " + (a + b));
	    System.out.println("a ja b korrutis on " + (a * b));
	    System.out.println("a ja b jagatis on " + (a / b));

		System.out.printf("%f ja %f summa on %f\n", a, b, (a + b));

		float c = 13000f;
		float e = 9000f;

		double f = 13000;
		double g = 9000;

		System.out.printf("%f ja %f jagatis on %f%n", c, e, (c / e));
		System.out.printf("%g ja %g jagatis on %g", f, g, (f / g));

		System.out.println(c / e);
		System.out.println(f / g);

		System.out.println(c * e);
		System.out.println(f * g);

    }
}
