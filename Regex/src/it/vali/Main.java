package it.vali;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        System.out.println("Sisesta email");
        Scanner scanner = new Scanner(System.in);

        String email = scanner.nextLine();
        String regex = "([a-z0-9\\._]{1,50})@[a-z0-9]{1,50}\\.([a-z]{2,10})\\.?([a-z]{2,10})?";
        if(email.matches(regex)) {
            System.out.println("Email oli korrektne");
        }
        else
        {
            System.out.println("Email ei olnud korrektne");
        }

        Matcher matcher = Pattern.compile(regex).matcher(email);
        if(matcher.matches())
        {
            System.out.println("Kogu email: " + matcher.group(0));
            System.out.println("Tekst vasakulpool @ märki: " + matcher.group(1));
            System.out.println("Domeeni laiend: " + matcher.group(2));
        }


        // Küsi kasutajalt isikukood ja valideeri, kas see on õiges formaadis
        // Mõelge ise, mis piirangud seal peaksid olemas
        // peab olema reaalne aasta, kuu number, kuupäeva number,
        // Küsi kasutajalt isikukood ja prindi välja tema sünnipäev

        System.out.println("Sisesta isikukood");
        String regexNew = "(^[1-6]{1})([0-9]{2})(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])([0-9]{4}$)";

        String personalID = scanner.nextLine();


        if (personalID.matches(regexNew)){
            System.out.println("Isikukood oli korrektne");
        }
        else
        {
            System.out.println("Isikukood ei olnud korrektne");
        }

        Matcher matcherNew = Pattern.compile(regexNew).matcher(personalID);

        if(matcherNew.matches()){
            System.out.println("Sugu(Mees - 3, naine - 4): " + matcherNew.group(1));
            System.out.println("Sünniaasta: " + matcherNew.group(2));
            System.out.println("Sünnikuu: " + matcherNew.group(3));
            System.out.println("Sünnipäev: " + matcherNew.group(4));
            System.out.println("Random kood: " + matcherNew.group(5));

        }
    }

}
