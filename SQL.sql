﻿-- See on lihtne hello, world teksti päring, mis tagastab ühe rea ja ühe veeru (veerul puudub pealkiri),
-- selles veerus ja reas saab olema tekst hello, world.
SELECT 'Hello, world';

-- Täisarvude puhul üksikuid jutumärke('') vaja pole. 
SELECT 3;

-- 'raud' + 'tee' ehk stringide liitmine sellisel kujul PostgreSQL'is ei tööta.
SELECT 'raud' + 'tee'

-- stringide liitmine toimub CONCAT meetodiga. Lisaks sõnadele saab kokku liita lisaks stringidele ka numbreid, mis automaatselt stringiks tehakse.
-- CONCAT toimub kõigis erinevates SQL serverites.
SELECT CONCAT('raud', 'tee', 2, 4);

-- Kui tahan erinevaid veerge, siis panen väärtustele koma vahele.
SELECT 'Peeter', 'Paat', 23, 75.45, 'Blond'

-- AS märksõnaga saab anda veerule nime.
SELECT
	'Peeter' AS Eesnimi, 
	'Paat' AS Perenimi, 
	23 AS Vanus, 
	75.45 AS Kaal, 
	'Blond' AS juustevärv,
	NOW() AS kuupäev,

-- Tagastab praeguse kuupäeva ja kellaaega mingis vaikimisi formaadis. 
SELECT NOW();

-- Kui tahan konkreetsed osa sellest, näiteks aastat või kuud
SELECT date_part('year', NOW())

-- Kui tahan mingit kuupäeva osa ise ette antud kuupäevast
SELECT date_part('month', TIMESTAMP '2019-01-01');

-- Kui tahan kellaajast saada näiteks minuteid.
SELECT date_part('minutes', TIME '10:10');

-- kuupäeva formaatimine Eesti kuupäeva formaati.
SELECT to_char(NOW(), 'HH24:mi:ss DD.MM.YYYY')

-- Interval laseb lisada või eemaldada mingit ajaühikut. (enne intervali commandi peab olema + või - märk)
SELECT now() + interval '2 centuries 3 years 2 months 1 weeks 3 days 4 seconds';

-- tabeli ehitamine
CREATE TABLE student (
	id serial PRIMARY KEY, 
	-- serial tähendab, et tüübiks on int, mis hakkab ühe võrra suurenema
	-- PRIMARY KEY (primaarvõti) on unikaalne väli tabelis.
	first_name varchar(64) NOT NULL, -- 64 sulgudes näitab maksimaalsete märkide arvu nimes, ei tohi väärtuseta olla (null).
	last_name varchar(64) NOT NULL,
	height int NULL, -- tohib tühi olla
	age int,
	weight numeric(5, 2) NULL, -- sulgudes olev esimene number näitab kohtade arvu ja teine number arvu täpsust peale koma
	birthday date NULL	
);
-- Tabelist kõikide ridade ja kõikide veergude küsimine
SELECT * FROM student;

-- Kui tahan filtreerida või otsida mingi tingimuse järgi, siis kasutan käsklust WHERE.
SELECT 
	* 
FROM 
	student;
WHERE 
	height = 180;
	
-- Küsi tabelist eesnime ja perekonnanime järgi mingi Peeter Tamm ja Mari Maasikas:
SELECT 
	* 
FROM 
	student
WHERE 
	(first_name = 'Peeter' AND last_name = 'Tamm')
	OR
	(first_name = 'Mari' AND last_name = 'Maasikas')
	
-- Anna mulle õpilased, kelle pikkus jääb 170-180cm vahele
SELECT 
	* 
FROM 
	student
WHERE 
	height >= 170 AND height <= 180

--Anna mulle õpilased, kelle sünnipäev on jaanuaris	
SELECT 
	first_name, height
FROM 
	student
WHERE 
	date_part('month', birthday) = 01
	
-- Anna mulle õpilased, kelle middle_name on null(määramata) - nulliga võrdlemisel kasutatakse IS käsklust. Võib-olla ka IS NOT NULL
SELECT 
	*
FROM 
	student
WHERE 
	middle_name IS NULL	

-- Anna mulle õpilased, kelle pikkus ei ole 180cm
SELECT 
	*
FROM 
	student
WHERE 
	height != 180
	
-- Anna mulle õpilased, kelle pikkus on 210, 140, 175 - selleks tuleb kasutada IN käsklust ja sulgudesse soovitud väärtused panna
SELECT 
	*
FROM 
	student
WHERE 
	height IN (210, 140, 175)
	
-- Anna mulle õpilased, kelle eesnimi ON Peeter, Mari või Kalle.
SELECT 
	*
FROM 
	student
WHERE 
	height IN ('Peeter', 'Mari', 'Kalle')

-- Anna mulle õpilased, kelle eesnimi EI OLE Peeter, Mari või Kalle.
SELECT 
	*
FROM 
	student
WHERE 
	height NOT IN ('Peeter', 'Mari', 'Kalle')			
	
-- Anna mulle õpilased, kelle sünnikuupäev on kuu esimene, neljas või seitsmes.	
SELECT 
	*
FROM 
	student
WHERE 
	date_part('day', birthday) IN (1, 4, 7)
	
-- Kõik WHERE võrdlused jätavad välja null väärtusega read ehk neid ei arvestata. Selleks tuleb kasutada IS NULL käsklust.
SELECT
	*
FROM
	student
WHERE
	height > 0 OR height <= 0 OR height IS NULL
	
-- Anna mulle õpilased pikkuse järjekorras lühemast pikemani.
SELECT
	*
FROM
	student
ORDER BY
	height
	
-- Anna mulle õpilased pikkuse järjekorras lühemast pikemani. Kui pikkused on võrdsed, järjesta kaalu järgi.
SELECT
	*
FROM
	student
ORDER BY
	height, weight
	
-- Kui tahan tagupidises järjekorras, siis lisandub sõna DESC (descending)
-- Olemas on ka ASC (Ascending), mis on vaikeväärtus.
SELECT
	*
FROM
	student
ORDER BY
	first_name DESC
	
-- Anna mulle vanuse järjekorras vanemast nooremaks õpilaste pikkused, 
-- mis jäävad 160 ja 170 vahele.	
SELECT
	*
FROM
	student
WHERE
	height >= 160 AND height <= 170	
ORDER BY
	birthday
	
-- Otsi kõik õpilased, kelle eesnimi algab, lõppeb või nime sees on mingi täht või sõnaühend.	
SELECT
	*
FROM
	student
WHERE
	first_name LIKE 'Ka%' -- see otsib, eesnimi algab KA-ga.
	OR first_name LIKE '%ee%' - see otsib, eesnimi sisaldab ee
	OR last_name LIKE '%aan' - see otsib, perenimi lõppeb aan-ga.
	
-- UPDATE lausega tabelis kirje muutmisel peab olema ettevaatlik (oht kõiki andmeid muuta). 
-- Alati peab kasutama WHERE lauset.
UPDATE
	student		
SET
	height = 199,
	weight = 85.56,
	middle_name = 'Kala'
WHERE
	id = 4

-- Muuda kõigi õpilaste pikkus ühe võrra suuremaks
UPDATE
	student
SET
	height = height + 1

-- Suurenda hiljem kui 1999 sündinud õpilastel sünnipäeva ühe päeva võrra.
UPDATE
	student
SET
	birthday =  birthday + interval '1 days'
WHERE 
	date_part('year', birthday) > 1999

-- Kustutamine - tuleb olla ettevaatlik, et kogu tabelit tühjaks ei tõmba. (Alati kasutada WHERE'i)
DELETE FROM
	student
WHERE
	id = 3

-- CRUD operations (Create (insert), Read (Select), Update, Delete) on eelnevad tegevused kokkuvõtva nimega.	


-- Loo uus tabel nimega Loan, millel on väljad: 
-- amount (reaalarv), 
-- start_date, 
-- due_date,
-- student_id,

CREATE TABLE loan (
	id SERIAL PRIMARY KEY,
	amount numeric(11, 2) NOT NULL, 
	start_date date NOT NULL,
	due_date date NOT NULL,
	student_id int NOT NULL
);

-- Tekitasime uue tabeli ning andsime väljadele väärtused. Omakorda sidusime info ära meie studenti tabeli student_id'dega.
INSERT INTO loan
	(amount, start_date, due_date, student_id)
VALUES	
	(13500.50, NOW(), '2020-01-01', 4),
	(213500.00, NOW(), '2024-02-06', 5),
	(1323500.40, NOW(), '2016-01-01', 6),
	(1233500.50, NOW(), '2020-01-01', 7),
	(131134500.50, NOW(), '2029-02-16', 8),
	(131134500.50, NOW(), '2022-08-19', 9)

-- Anna mulle kõik õpilased koos oma laenudega
-- INNER JOIN on sellne tabelite liitmine, kus liidetakse ainult need read, kus on võrdsed student.id = loan.student_id ehk
-- need read, kus mõlemas tabelis on tabelite vahel seos. Ülejäänud read ignoreeritakse. 
SELECT 
	student.first_name,
	student.last_name,
	loan.amount
FROM
	student
INNER JOIN -- INNER JOIN on vaikimisi join, INNER sõna võib ära jätta.
	loan
	ON
	student.id = loan.student_id
 
-- Anna mulle kõik õpilased koos oma laenudega, aga ainult sellised laenud, mis on suuremad, kui 500
-- Järjesta laenu suuruse järgi.
SELECT 
	student.first_name,
	student.last_name,
	loan.amount
FROM
	student
JOIN
	loan
	ON
	student.id = loan.student_id
WHERE
	loan.amount > 500
ORDER BY
	loan.amount DESC 
	
-- Loome uue tabeli loan_type, milles on väljad: name ja description
CREATE TABLE loan_type (
	id SERIAL PRIMARY KEY,
	name varchar(30) NOT NULL,
	description varchar(500) NULL
);

-- Käsuga ALTER saab muuta tabeli andmeid ning seejärel tuleb konkreetselt see, mida me tabelis teeme.
ALTER TABLE loan
ADD COLUMN loan_type_id int

-- http://www.postgresqltutorial.com/postgresql-add-column/

-- Tabeli kustutamine
DROP TABLE student;	

-- Lisame loan_type tabelile uued laenutüübid ja nende kirjeldused
INSERT INTO loan_type
	(name, description)
VALUES
	('Õppelaen', 'See õpilastele mõeldud väga hea laen'),
	('SMS laen', 'Kehv viis raha laenata'),
	('Kodulaen', 'Saad elamise osta selle laenuga'),
	('Väikelaen', 'Saad uued rehvid osta oma autole') 
	
-- Tabeleid saame defineerida ka üksikute tähtedena, millele viidates saame erinevaid käsklusi läbi viia.
-- Kolme tabeli INNER JOIN
SELECT
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
	
-- LEFT JOIN'i puhul võetakse joini esimesest (vasakust) tabelist kõik read ning teises tabelis (paremast) näidatakse puuduvatel kohtadel null.
SELECT
	s.first_name, s.last_name, l.amount
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id	
			
-- LEFT JOIN'i puhul on järjekord väga oluline
SELECT
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
LEFT JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id	
	
-- CROSS JOIN annab kõik kombinatsioonid kahe tabeli vahel.
SELECT
	s.first_name, s.last_name, l.amount
FROM
	student AS s
CROSS JOIN
	loan AS l	
	
--FULL OUTER JOIN on sama, mis LEFT JOIN + RIGHT JOIN
SELECT
	s.first_name, s.last_name, l.amount
FROM
	student AS s
FULL OUTER JOIN
	loan AS l
	ON s.id = l.student_id

-- Anna mulle kõigi kasutajate perekonnanimed, kes võtsid SMS laenu ja kelle laenu suurus on üle 100€. Tulemused järjesta laenu võtja vanuse järgi väiksemast suuremani.

--AGGREGATE FUNCTIONS

-- Agregaatfunktsiooni selectis välja kutsudes kaob võimalus samas select lauses küsida mingit muud välja tabelist, sest agregaatfunktsiooni tulemas on alati ainult 1 number.
-- ja seda ei saa kuidagi näidata koos väljadega, mida võib olla mitu rida.

-- Palju on iga õpilane keskmiselt laenu võtnud:
-- Arvesse lähevad ka õpilased, kes pole laenu võtnud.
SELECT
	AVG(COALESCE(loan.amount, 0)) -- COALESCE funktsiooniga anname tulba, milles olevaid NULL väärtusi saame asendada 0-ga (või
							 -- misiganes väärtusega)
FROM
	student
LEFT JOIN	
	loan
	ON student.id = loan.student_id
	
-- Erinevad agregaatfunktsioonid
SELECT
	ROUND(AVG(COALESCE(loan.amount, 0)),0) AS "Keskmine laenusumma",
	MIN(loan.amount) AS "Minimaalne laenusumma",
	MAX(loan.amount) AS "Maksimaalne laenusumma",
	COUNT(*) AS "Kõikide ridade arv",
	COUNT(loan.amount) AS "Laenude arv", -- jäetakse välja read, kus loan.amount on NULL
	COUNT(student.height) AS "Mitmel õpilasel ON pikkus"
FROM
	student
LEFT JOIN	
	loan
	ON student.id = loan.student_id	
	
-- Kasutades GROUP BY jäävad SELECT päringu jaoks alles vaid need väljad, mis on GROUP BY's välja toodud(s.first_name ja s.last_name)
-- Teisi välju saab ainult kasutada agregaatfunktsioonide sees.
SELECT
	s.first_name, 
	s.last_name, 
	SUM(l.amount),
	ROUND(AVG(l.amount), 2),
	MIN(l.amount),
	MAX(l.amount)
	
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	s.first_name, s.last_name

-- Anna mulle laenude summa laenutüüpide järgi
SELECT
	lt.name,
	SUM(l.amount)
FROM
	loan AS l
JOIN
	loan_type AS lt
	ON l.loan_type_id = lt.id
GROUP BY
	lt.name	
	
-- Anna mulle laenude summad sünniaastate järgi.
SELECT
	date_part('year', s.birthday), SUM(l.amount)	
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part('year', s.birthday)	

-- Tekita mingile õpilasele 2 sama tüüpi laenu
-- Anna mulle laenude summa grupeerituna õpilase ning laenu tüübi kaupa
SELECT
	s.first_name, s.last_name, lt.name, SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON l.student_id = s.id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
GROUP BY
	s.first_name, s.last_name, lt.name

-- Anna mulle mitu laenu mingist tüübist on võetud ning mis on nende summa.
-- Tulemus peaks olema, et "Laenu tüüp" + "Laenude arv" + "Summa kokku"
SELECT
	lt.name, COUNT(lt.id), SUM(l.amount)
FROM
	loan_type AS lt
RIGHT JOIN
	loan AS l
	ON lt.id = l.loan_type_id
GROUP BY	
	lt.name

-- Mis aastal sündinud võtsid suurima summa laene?
SELECT
	date_part('year', birthday), SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part('year', birthday)
ORDER BY 
	SUM(l.amount) DESC
LIMIT 1	-- Annab ainult ühe väärtuse

-- Anna mulle õpilaste eesnime esitähtede esinemise statistika. Mitme õpilase eesnimi algab mingi tähega?
SELECT SUBSTRING
	(s.first_name, 1, 1), COUNT((s.first_name, 1, 1))
FROM
	student AS s
GROUP BY SUBSTRING
	(s.first_name, 1, 1)
	
-- Stringi meetodid: http://www.postgresqltutorial.com/postgresql-string-functions/

-- Anna mulle õpilased, kelle nimi on keskmise pikkusega õpilaste keskmine nimi. 
-- Subquery or inner query or nested quary - üks SELECT teise SELECT'i sees.
SELECT
	first_name, last_name, (SELECT weight FROM student WHERE id = 8)
FROM
	student
WHERE 
	first_name IN 
(SELECT
	middle_name
FROM
	student
WHERE
	height = (SELECT ROUND(AVG(height)) FROM student)) 


-- Lisa kaks vanimat õpilast töötajate tabelisse
-- ÜHEST TABELIST TEISE LISAMISE NÄIDE
INSERT INTO employee (first_name, last_name, birthday, middle_name)
SELECT first_name, last_name, birthday, middle_name FROM student ORDER BY birthday LIMIT 2		

-- Teisendamine täisarvuks
SELECT CAST(ROUND(AVG(age)) AS UNSIGNED) FROM emp99;	
	

	
	