package it.vali;

public class Main {

    static int a = 3;

    public static void main(String[] args) {
	    int b = 7;


        a = 0;

        increaseByA(10);
        System.out.println(b + a);
        System.out.println(increaseByA(10));

        // Shadowing, ehk siis sees poole defineeritud muutuja varjutab väljaspool oleva muutuja
        int a = 6;


        // Selle käsklusega saab muuta klassi muutuja väärtust. Main.a käsklusega pöördutakse klassi muutuja poole ja väärtustatakse see ümber.
        Main.a = 8;

        System.out.println(b + a);
        System.out.println(increaseByA(10));
    }

    static int increaseByA(int b){
        // Siin kasutatakse kõige välimist a muutujat ehk seda, mis on Main classi küljes. Kui kirjutame siia
        // meetodi alla uue a muutuja, siis kasutatakse seda väärtust, mille meetodi alla panime.
        return b += a;
    }
}
