package it.vali;

public class Main {

    public static void main(String[] args) {
        String sentence = "Elas metsas Mutionu keset kuuski noori vanu";

        //Sümbolite indeksid tekstis algavad samamoodi indeksiga 0 nagu massiivideski

        //Leia üles esimene tühik ehk mis on tema indeks/asukoht:
        //Loome muutuja spaceIndex ja võrdsustame ta meie senctence stringi asukohaga, kust algab sõna "metsas"
        int spaceIndex = sentence.indexOf(" ");
        //indexOf tagastab -1, kui otsitavat fraasi/sümbolit uuritavas stringis ei leitud.
        System.out.println(spaceIndex);

        int secondSpaceIndex = sentence.indexOf(" ", spaceIndex + 1);
        System.out.println(secondSpaceIndex);

        //Prindi välja kõigi tühikute indexid lauses:
        while (spaceIndex != -1) {
            System.out.println(spaceIndex);
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
        }

        System.out.println();

        spaceIndex = sentence.lastIndexOf(" ");
        while (spaceIndex != -1) {
            System.out.println(spaceIndex);
            spaceIndex = sentence.lastIndexOf(" ", spaceIndex - 1);
        }

        System.out.println();
        //Teine indeksi oluline meetod on subString ja sellega saab otsida sõna osa.

        //Otsi ja prindi välja lause teine sõna:
        spaceIndex = sentence.indexOf(" ", 0);
        int secondsSpaceIndex = sentence.indexOf(" ", spaceIndex + 1);

        String secondWord = sentence.substring(spaceIndex + 1, secondsSpaceIndex);
        System.out.println(secondWord);


        //Leia esimene k-tähega sõna:
        int kIndex = sentence.indexOf(" k") + 1; //leiame tühik + k asukoha ning liidame sellele kohe 1 juurde, et otsimist alustatakse hlijem pärast k tähte
        spaceIndex = sentence.indexOf(" ", kIndex); //siin otsime tühikut alates KIndexist

        String KWord = sentence.substring(kIndex, spaceIndex);
        System.out.println(KWord);


        //Leia mitu sõna on lauses:
        int spaceCounter = 0;
        spaceIndex = sentence.indexOf(" ");
        while (spaceIndex != -1) {
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
            spaceCounter++;
        }
        System.out.printf("Sõnade arv lauses on %d%n", spaceCounter + 1);


        // Leia, mitu k-tähega algavat sõna on lauses
        sentence = "Elas metsas Mutionu keset kuuski noori vanu"; //kopisin uuesti siia, defineeritud üleval

        int kCounter = 0;
        sentence = sentence.toLowerCase();  //teeme oma esialgse sentence'i stringi kõik sõnad väikesteks tähtedeks
        kIndex = sentence.indexOf(" k");    //loome kIndexi muutuja ning väärtustame ta muutuja " k" asukohaga meie stringis

        while (kIndex != -1) {    //seni kuni kIndexi väärtus ei ole -1 ehk ta eksisteerib stringi mingis asukohas (-1 tähendaks, et ei eksisteeri)
            kIndex = sentence.indexOf(" k", kIndex + 1);    //kIndex saab tsüklis uusi väärtusi
            kCounter++;
        }
        if (sentence.substring(0, 1).equals("k")) {   //kontrollime erijuhtu stringis, kui meie sentece'i kõige esimene täht on k.
            kCounter++;
        }
        System.out.printf("K-tähega algavate sõnade arv lauses on %d%n", kCounter);


        // Loe üle kõik sõnad, mis sisaldavad a tähte
        // Prindi välja kõik sõnad, kus on neli tähte
        // Prindi välja kõige pikem sõna
        // Prindi välja sõna, kus esimene ja viimane täht on sama


        // Prindi kõik a-tähega lõppevad sõnad
        sentence = "Elas metsas Mutionu keset kuuski noori vanu";

        for (int i = 0; i < sentence.length(); i++) {
            if (sentence[i].substring(sentence[i].length-1, 1).equals("a")) {
                System.out.println(sentence[i]);

            }
        }
    }
}