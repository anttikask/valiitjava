package it.vali;

public class Main {

    public static void main(String[] args) {
        // Stringide tükeldamine ja liitmine
        String[] words = new String[]{"Põdral", "maja", "metsa", "sees", "mees"};
        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }


        System.out.println();

        //Prindi kõik sõnad massiivist, mis algavad m-tähega:
        for (int i = 0; i < words.length; i++) {
            if (words[i].substring(0, 1).equals("m")) {
                System.out.println(words[i]);
            }
        }

        //Prindi kõik a tähega lõppevad sõnad
        for (int i = 0; i < words.length; i++) {

            String lastLetter = words[i].substring(words[i].length() - 1, words[i].length());

            if (lastLetter.toLowerCase().equals("a")) {
                System.out.println(words[i]);
            }
        }


        //Loe üle kõik sõnad, mis sisaldavad a tähte
        int counter = 0;
        for (int i = 0; i < words.length; i++) {
            if (words[i].indexOf("a") != -1) {
                counter++;
            }
        }
        System.out.println(counter);

        System.out.println();


        //Prindi välja kõige pikem sõna
        String longestWord = words[0];  //väärtustame uue stringi meie massiivi esimese elemendiga

        for (int i = 1; i < words.length; i++) {   //kontrollime kõigi massiivi elemente
            if (words[i].length() > longestWord.length()) { //kui massiivi element kohal i on pigekm meie stringi longestWordi pikkusest
                longestWord = words[i]; //sel juhul võrdsustame stringi elemendi kohal i pikima sõna stringiga
            }
        }
        System.out.println(longestWord);


        System.out.println();


        //Prindi välja sõnad, kus on esimene ja viimane täht samad
        for (int i = 1; i < words.length; i++) {
            if (words[i].substring(0, 1).equals(words[i].substring((words[i].length() - 1)))) {
                System.out.println(words[i]);
            }
        }
    }
}
