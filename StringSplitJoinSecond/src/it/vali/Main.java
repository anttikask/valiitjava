package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Split meetod tükeldab stringi ette antud sümbolite kohalt ja tekitab sõnade massiivi
        // | tähendab regulaaravaldises VÕI
        String sentence = "Väljas on ilus ilm, vihma ei saja ja päike paistab";
        String[] words = sentence.split(" ja | |, "); // teeb uue massiivi nimega words ning väärtustab selle sentece'i
                                                            // osad sõnadega split meetodil
                                                            // pannes eraldajaks spliti järel sulgudes oleva sümboli.
        for (int i = 0; i < words.length ; i++) {
            System.out.println(words[i]);
        }


        System.out.println();


        // Join on staatiline meetod, mis teeb massiivist stringi
        // Tekitame uue stringi nimega newSentence ning kasutame staatilist meetodit join,
        // et massiivist nimega words see string teha (eraldajaks tühik vms. delimiter)
        String newSentence = String.join("", words);

        System.out.println(newSentence);



        // Sama ülesanne, delimiteriks pandud reavahetus
        newSentence = String.join("\n", words);

        System.out.println(newSentence);




        // Escapingu (kõik escape sümbolid algavad \-ga) näited:    // https://www.freeformatter.com/java-dotnet-escape.html -
                                                                    // escape sümbolite ülevaade

        System.out.println("Juku ütles: \"Mulle meeldib suvi\"");   // Kaldkriips on escape symbol ja see on sini vajalik, et jutumärke
                                                                    // teksti sees välja printida.

        System.out.println("C:\\Users\\opilane\\Documents");        // Et faili asukohta välja printida tuli lisada üks kaldkriips, vastasel
                                                                    // juhul ei saa üksikuid kaldkriipse välja printida.

        System.out.println("Juku\b ütles: \"Mulle meeldib suvi\""); // \b kustutab ära talle eelneva tähe, sümboli.


        System.out.println(); //Eraldusrida



        // Küsi kasutajalt rida numbreid nii, et ta paneb need numbrid kirja ühele reale eraldades tühikuga. Seejärel liidab
        // kõik numbrid kokku ja seejärel prindib vastuse.

        Scanner scanner = new Scanner(System.in);
        System.out.println("Sisesta arvud, mida tahad omavahel liita, eraldades need tühikuga");

        String numbersText = scanner.nextLine();
        String[] numbers = numbersText.split(" "); // koostame uue stringi, mille väärtuseks on kasutaja input splititud tühikutega

        int sum = 0;    //tekitame summa muutuja
        for (int i = 0; i < numbers.length ; i++) {     // käime läbi i muutujad kuni stringi massiivi numbers pikkuseni.
            sum = sum + Integer.parseInt(numbers[i]);   // summa muutujale liidame otsa summe ning numbers[i] väärtuse, mille peame int'iks tegema
        }
        String joinedNumbers = String.join(", ", numbers);  // tekitame uue stringi ning liidame massiivi number kokku eraldusmärgiks
                                                                    // on ", "
        System.out.printf("Arvude %s summa on %d%n", joinedNumbers, sum);
    }
}
