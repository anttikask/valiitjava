package it.vali;

import java.lang.reflect.Array;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    int a = 0;
        try {
            int b = 4 / a;
            String word = null;
            word.length();


        }
        // Kui on mitu Catch plokki, siis otsib ta esimese ploki, mis oskab antud exceptioni kinni püüda.
        catch (ArithmeticException e) {
            if (e.getMessage().equals("/ by zero")){
                System.out.println("Nulliga ei saa jagada");
            }
            else {
                System.out.println("Esines aritmeetiline viga");
            }
        }
        catch (RuntimeException e){
            System.out.println("Reaalajas esinev viga");
        }
        // Exception'i üldine tüüp on klass, millest kõik erinevad Exceptioni tüübid pärinevad.
        // See omakorda tähendab, et püüdes kinni üldise Exceptioni, püüame kinni kõik Exceptionid.
        catch (Exception e) {
            System.out.println("Esines viga");
        }

        // Küsime kasutajalt numbri ja kui see number ei ole korrektne, siis ütleme veateate
        Scanner scanner = new Scanner (System.in);

        boolean correctNumber = false;
        do {
            System.out.println("Ütle üks number");
            try {
                int c = Integer.parseInt(scanner.nextLine());
                correctNumber = true;
            }
            catch (NumberFormatException e) {
                System.out.println("Esines viga");
            }
        } while (!correctNumber);

        // Loo täisarvude massiiv 5 täisarvuga ning ürita sinna lisada 6. täisarv.

        int[] array = new int[5];

        try {
            array[0] = 2;
            array[1] = 5;
            array[2] = 6;
            array[3] = 4;
            array[4] = 2;
            array[5] = 1;
        } catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Liiga palju arve");
        }
    }
}
