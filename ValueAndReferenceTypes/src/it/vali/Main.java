package it.vali;

import java.io.PipedInputStream;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	    // Kui panna üks primitiiv tüüpi muutuja võrduma teise muutujaga, siis tegelikult tehakse arvuti mällu uus
        // muutuja ja väärtus kopeeritakse sinna.

        // Sama asi juhtub ka primitiiv tüüpi muutuja kaasa andmisel meetodi parameetriks, iis tegelikult tehakse arvuti mällu uus
        // muutuja ja väärtus kopeeritakse sinna.
        int a = 3;
	    int b = a;
	    a = 7;
        System.out.println(b);

        increment(b);
        System.out.println(b);

        // Kui panna reference (viittüüpi) type muutuja võrduma teise muutujaga, siis tegelikult jääb mälus ikkagi alles
        // ainult üks muutuja, lihtsalt teine muutuja hakkab viitama samale kohale mälus (samale muutujale).
        // Meil on 2 muutujat, aga tegelikult nad on täpselt sama objekt.
        int[] numbers = new int[] {-5, -2, 4, 6};

        int[] secondNumbers = numbers;
        numbers[0] = 3;

        System.out.println(secondNumbers[0]);

        Point pointA = new Point();
        pointA.x = 10;
        pointA.y = 3;

        Point pointB = pointA;
        pointB.x = 7;

        System.out.println(pointA.x);

        List<Point> points = new ArrayList<Point>();
        points.add(pointA);
        points.add(pointB);

        pointA.y = -4;
        System.out.println(points.get(0).y);
        System.out.println(points.get(1).y);
        System.out.println(pointB.y);

        pointA.printNotStatic();

        Point.printStatic();

        Point pointC = new Point();
        pointC.x = 12;
        pointC.y = 20;
        increment(pointC);
        System.out.println(pointC.x);
        System.out.println(pointC.y);

        increment(numbers);
        System.out.println(numbers);

        Point pointD = new Point();
        pointD.x = 5;
        pointD.y = 6;
        pointD.increment();
        System.out.println(pointD.y);

    }

    public static void increment(int a) {
        a++;
        System.out.printf("a on nüüd %d%n", a);
    }

    // Suurendame x ja y koordinaati 1 võrra
    public static void increment(Point point){
        point.x++;
        point.y++;
    }

    // Suurendame kõiki elemente ühe võrra.
    public static void increment(int[] numbers) {
        for (int i = 0; i < numbers.length ; i++) {
            numbers[i]++;
            System.out.println(numbers[i]);
        }

    }


}
