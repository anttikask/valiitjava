package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //Prindime i välja 1-st kuni 5-ni
        for (int i = 1; i <= 5; i++) {
            System.out.println(i);
        }

        //Sama asi ainult kasutades while loopi
        int i = 1;
        while (i<=5){
            System.out.println(i);
            i++;
        }

        //Küsi kasutajalt, mis päev täna on seni kuni ta ära arvab
        String answer = "";
        Scanner scanner = new Scanner(System.in);

        while (!answer.toLowerCase().equals("neljapäev")) {
            System.out.println("Mis päev täna on?");
            answer = scanner.nextLine();
        }
        System.out.println("Täpselt nii");

        //Sama ülesanne for tsükliga:
        for(; !(scanner.nextLine().equals("neljapäev")) ; ) {
            System.out.println("Mis päev täna on?");
            answer = scanner.nextLine();
        }
    }
}
