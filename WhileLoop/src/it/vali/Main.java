package it.vali;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) {
// while tsükkel on tsükkel, kus korduste arv ei ole teada. For tsükli puhul oli teada.

        //Luuakse Random objekt, mille nimeks saab random
        Random random = new Random();
        //random.nextInt(5) genereerib random number, mille bound on 0-5
        int number = random.nextInt(5) + 1;

          //Teine võimalus sama random numbrit tekitada.
//        int randomNum = ThreadLocalRandom.current().nextInt(1, 6);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Arva ära üks number 0-5");
        int guess = Integer.parseInt(scanner.nextLine());

        do {
            System.out.println("Arva ära üks number 0-5");
        }

        while(guess != number); {
            System.out.println("Proovi uuesti!");
            guess = Integer.parseInt(scanner.nextLine());
        }
        System.out.println("Tubli, arvasid numbri ära!");
    }

}

