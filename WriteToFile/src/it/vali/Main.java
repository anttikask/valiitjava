package it.vali;

import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        // Try plokis otsitakse/oodatakse Exceptioneid (Erind, Erand, Erijuht, Viga)
        try {
            // FileWriter on selline klass, mis tegeleb faili kirjutamisega
            // Sellest klassist objekti loomisel antakse talle ette faili asukoht
            // Faili asukoht võib olla ainult faili nimega kirjeldatud: output.txt
            // Kui ma asukohta ei ütle siis sel juhul kirjutatakse faili, mis asub samas kaustas kus meie Main.class
            // või täispika asukohaga kirjeldatud e:\\users\\opilane\\documents\\output.txt
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\output.txt");

            fileWriter.write("Elas metsas Mutionu" + System.lineSeparator());   //kirjutatakse eelnevalt välja toodud dokumenti vastav tekst koos line spearatoriga
            fileWriter.write("Tere");

            fileWriter.close(); // Kindlasti tuleb fileWriter lõpetada close meetodiga.

        // Catch plokis püütakse kinni kindlat tüüpi Exception või kõik Exceptionid, mis pärinevad antud Exceptionist.
        //
        } catch (IOException e) {
            // printStackTrace tähendab, et prinditakse välja meetodite välja kutsumise hierarhia/ajalugu
            // e.printStackTrace();
            System.out.println("Viga: antud failile ligipääs ei ole võimalik");
        }

    }
}
